# Netflow Parallel

Parallelization of core functions in the netflow Python library.

PETSc installation:
`./configure --with-cc=/usr/bin/mpicc --with-cxx=/usr/bin/mpicxx --with-fc=0 --download-hypre`

# TODO:
- Overlap communication between main and worker threads with computation
- Decompose neighbor pores to be processed to reduce communication frequency
