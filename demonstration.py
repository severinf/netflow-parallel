# Demonstration of multi-threaded network generation and subsequent
# distributed flow simulation (MPI).
#
# Resources (CPU cores) are 'shared' between MPI ranks used in
# netflow.solve_flow_periodic() and multiprocessing threads employed in
# netflow.generate_dendrogram(). This means netflow.generate_dendrogram()
# is ran on a single (root) rank, spawning additional threads that are
# mapped to free CPU cores (not already bound to remaining MPI ranks).
#
# To ensure threads are not all mapped to the same core as the root rank,
# use mpirun option --bind-to none. Alternatively, use more specific
# binding with --bind-to core e.t.c.
#
# For 4 cores use e.g. 2 ranks and 3 threads (root rank is also a thread)
# -> as is the case here
#
# For 36 cores use e.g. 13 ranks and 24 threads ...
# e.t.c.
# 
# Severin Fritschi
# January 2022

# Calls MPI_Init_thread
from mpi4py import MPI

import sys
# Add directory including compiled binary to path
sys.path.append("netflow/")

from netflow import *

def main():
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()
    # Root has to be rank 0 (used for initialization of linear system)
    root = 0
    
    if (size != 2):
        if (rank == root):
            print("Run with exactly 2 ranks.")
            print("Usage: mpirun -n 2 --bind-to none python3 demonstration.py")
        comm.Abort(1)
        
    # Generate dendrogram-based network (root-only)
    dendronet = None
    if (rank == root):
        # Number of threads used for network generation
        nthreads = 3
        # Target size of network to be generated
        target = [2, 2, 2]
        print("Generating network of size {} on root".format(target))
        print("Using {} thread(s)".format(nthreads))
        # Load base network used for generation
        basenet = netflow.load_network_from("network/network.h5")
        # Cut-off is half maximal dimension in base network
        cutoff  = 0.5 * max([basenet.ub[i] - basenet.lb[i] \
                            for i in range(len(basenet.ub))])
        start = MPI.Wtime()
        dendronet = netflow.generate_dendrogram(basenet=basenet, targetsize=target, \
                                        cutoff=cutoff, sd=42, nthreads=nthreads, \
                                        mute=True)
        end = MPI.Wtime()
        elapsed = end - start
        print("Elapsed time: %e s\n" % elapsed)
        # No longer needed
        del basenet
        
    # Wait for root rank
    comm.Barrier()
    
    # Simulate flow using PETSc on generated network using 2 MPI processes
    # - Flow is in z-direction with a pressure gradient of 100 Pa/m
    #   and dynamic viscosity mu = 0.001 Pa s
    if (rank == root):
        print("Simulating periodic network flow with PETSc")
    
    start = MPI.Wtime()
    p, Q = netflow.solve_flow_periodic(network=dendronet, \
                        solver=netflow.Solver.PETSC, mu=1e-3, c=3, P2L=1e2)
    end = MPI.Wtime()
    
    if (rank == root):
        print("Elapsed time: %e s" % (end - start))

if __name__ == '__main__':
    main()
