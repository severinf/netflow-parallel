\documentclass[10pt]{article}

\title{\textbf{Generation and Simulation of Large-Scale Flow Networks}}
\author{Severin Fritschi}
\date{}

% packages
\usepackage[backend=biber, sorting=none]{biblatex}
\usepackage{caption}
\usepackage{subcaption} % sub-figures
\usepackage[labelfont=bf]{caption, subcaption} % figure captions (bold-face)
\usepackage{multicol}  % multicolumn environment
\usepackage{tikz} % figures
\usepackage{xcolor} % color for cell lists picture
\usepackage[bottom]{footmisc} % footnotes below figures
\usepackage{hyperref} % web-links
\usepackage{listings} % line break in verb environment
\usepackage{amsmath} % math commands
% packages for algorithms
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{pdfpages} % declaration of originality

% argmin
\DeclareMathOperator*{\argmin}{arg\min}

% references
\addbibresource{bib/RandomGenerationFlowNetworks.bib}
\addbibresource{bib/netflow.bib}
\addbibresource{bib/petsc.bib}
\addbibresource{bib/hypre.bib}

\begin{document}
	\maketitle
	\begin{center}
		\large{Study Programme Computational Sciences and Engineering}
	
		\vspace{20ex}
		\large{Bachelor Thesis HS 21}
	
		\vspace{5ex}
		\large{Institute of Fluid Dynamics}
	
		\large{ETH Zürich}
	
		\vspace{20ex}
		\begin{tabular}{ l l }
			\large{Supervisor:} & \large{Daniel W. Meyer} \\
			\large{Professor:} & \large{Patrick Jenny}
		\end{tabular}
	\end{center}

	\newpage
	
	\begin{center} \Large{\textbf{Abstract}} \end{center}
	To study the flow properties of large void-space geometries found in porous media such as f.ex. soil or gravel, \cite{MEYER2021103936} describes and implements routines for the generation \& simulation of flow networks in the Python library \emph{netflow} \cite{MEYER2021101592}. Based on a relatively small base network acquired via tomographic scans, the generated flow network is of intermediate size (millions of pores). To extend this procedure to even larger networks (up to 100 millions of pores), parallel computing is employed for both generation of pore-networks as well as solving the flow for said networks. In the latter, we rely on existing MPI-based parallel solvers from the PETSc \cite{petsc-web-page} toolkit. See Appendix ~\ref{appendix:install} for installation details.

	\vspace{5ex}
	\begin{multicols}{2}
    	\section{Introduction}
    	\hspace{0.5cm}Porous media are abundant in nature. Various types of soils harbor intricate networks that enable the flow of groundwater and subsequently the transport of important chemical compounds through the soil. To understand these natural phenomena, it is of key interest to study the flow through pore networks. However, this requires sufficiently large void-space geometries taken from porous bodies, which despite advances in scanning technologies, is infeasible. Additionally, in pursuit of simulation efficiency, the 3D images obtained from scans are converted into a simplified representation consisting of spherical pores (nodes), connected by cylindrical throats (edges). To overcome above size limitation, the paper from \cite{MEYER2021103936} outlines a procedure for generating random realizations of much larger flow networks, taking an existing base network (See Appendix ~\ref{appendix:base}), obtained from a scan of a porous medium, as input. Furthermore, this method is particularly useful for generating heterogeneous networks, characterized by an irregular pore distribution. This is done with dendrogram-based clustering of pores in the original network, followed by random rotations of said clusters. When performed repeatedly and arranged in a 3-dimensional grid of perturbed base network copies, the resulting network, carrying over pore statistics such as coordination number \& radius, preserves the irregular pore structure of the original network. The \emph{netflow} package that implements the mentioned algorithm also provides functionality for solving and analyzing the flow through such networks. The former of which is now to be parallelized in a distributed fashion, so that it may support larger networks with up to 100 millions of pores. In a further step, the network generation algorithm shall be complemented by a parallel shared memory approach, specifically for connecting the generated pores in the resulting network.
    
		\section{Parallel Flow Solver}
		\label{sec:solver}
		\subsection{PETSc Interface}
		\hspace{0.5cm}Despite the availability of Python package \emph{petsc4py}, we decided to use the native implementation in C instead. This choice was motivated by the fact that the latter is better maintained, and the inclusion of an additional Python module would further complicate the dependency tree on the Python side. In order to interface the chosen C API of PETSc with the \emph{netflow} Python module, we rely on Cython to wrap the C source in Python, that is subsequently compiled with all required compilation and linking flags of PETSc. This allows us to invoke a \verb|solve_py()| function from Python delegating the relevant parameters, namely the system matrix and right-hand-side vector, to the C function \verb|solve()|. Here, the system matrix, in the compressed row storage format, is converted into PETSc's internal representation for sparse and distributed matrices called \verb|Mat|. The same applies to the r.h.s. which is used to initialize a distributed vector object in PETSc, i.e. \verb|Vec|. When the solution has been computed, it is communicated in full to the root rank via a call to \verb|MPI_Gatherv()|. It is stored in a Cython memoryview, converted back into a numpy array, and finally returned by \verb|solve_py()|.
		\subsection{Solver}
	    \hspace{0.5cm}The actual solver written in C then utilizes PETSc's collection of krylov-subspace (KSP) methods to iteratively approximate the solution of the system in parallel with the available MPI processes. To avoid data duplication, the initial assembly of PETSc objects is only done on the root rank and then communicated in parts to the corresponding ranks through PETSc's collective \verb|Assembly| routines. The iterative method chosen to solve the non-symmetric pressure system, arising from the flow network, is GMRES together with a left algebraic multi-grid preconditioner supplied via hypre \cite{hypre-web-page}.
	    \subsection{Limitations}
	    \hspace{0.5cm}Since the datastructure used to represent the pores in \emph{netflow} is a Python \verb|set|, the order of the pores is arbitrary. In particular, this means each MPI process sees a different ordering from eachother, which necessitates initialization of the full system matrix \& r.h.s. on a given root rank, such that the ordering is consistent for all ranks. This requires additional communication, but prevents duplication of the data associated with the matrix etc. on the remaining processes.
	    \subsection{Results}
	    \label{sec:quality}
	    \hspace{0.5cm}In order to assess the quality of the pressure-solution obtained by this solver, we study the fluxes induced by the pore pressures for a given base network comprised of 2636 pores. In particular, we look at all in- and out-going fluxes as well as their sum on a per pore basis (except for source/sink pores), obtained from the function \verb|flux_balance()|. When we complete this analysis for all available solvers and compare the results of the parallel PETSc solver with the existing single-core solvers, we see that the parallel version is in complete agreement with the rest in terms of solution quality, as depicted in Figure ~\ref{fig:balance}.
	
	\end{multicols}
	
	\begin{figure}[ht]
		\vspace{-0.5cm}
		\centering
		\includegraphics[width=0.8\textwidth]{plots/flux_PETSC.png}
		\caption{Pressures $p_{\mathrm{in}}$ and $p_{\mathrm{out}}$ are applied to in-pores and out-pores respectively, driving the network flow. The resulting pressure system is solved with the available solvers and the different fluxes are shown for the individual pores in the case of PETSc (using 4 processes). Since the sequential solvers produce an identical plot, they are omitted here. }
		\label{fig:balance}
	\end{figure}
	
	\begin{multicols}{2}
		\section[Parallel Network Generation]{Parallel Network\\ Generation}
		\vspace{-0.25cm}
		\hspace{0.5cm}The existing serial dendrogram-based network generation algorithm, as presented in \cite{MEYER2021101592}, is now modified. Concretely, to allow connecting all pores that populate the larger, generated network domain in parallel, we rely on a shared memory approach via the \verb|multiprocessing| Python module. To accomplish this, we first shift our attention to \emph{cell lists}, which offer a direct application of the already existing \textbf{maximal throat length} parameter $L_m$ as a suitable \textbf{cell size}.
		\subsection{Cell Lists}
		\hspace{0.5cm}To speed up neighbor search for the \emph{stationary} pores, we have opted to use the well-known cell lists data structure instead of the triangulation based approach outlined in \cite{MEYER2021103936}. This choice is supported by the useful properties of cell lists for our purpose of generating a network of similar pore-arrangement. It is also favorable over the triangulation method as the cell lists are only initialized once, and pores that have already been processed by the algorithm can be removed efficiently. Primarily however, it allows a straight-forward application of parallel computing, by distributing the work needed to find the neighbors of individual pores evenly. See Figure ~\ref{fig:cell} for a visualization.
		\subsection{Multiprocessing}
		\hspace{0.5cm}Using the \verb|multiprocessing| module, we can initialize a \emph{shared memory} region in the form of a \verb|RawArray| to store the indices\footnote{RawArray only supports primitive data types, therefore indices are used to uniquely identify every pore.} of best matching neighbor pores for all throats of any given pore. This so-called pore-match table or array is filled, massively in parallel by the individual threads, without any need for synchronization, as each process only operates on its dedicated part. This has excellent performance implications and is discussed in more detail in section ~\ref{sec:perf}. A \textbf{single thread} then iterates over its assigned pores, and \textbf{among all neighboring pores} contained within adjacent cells of the current pore, finds for each throat, carried over from the base network, the closest match. Ignored as possible match candidates are, trivially, the pore itself, any pore \emph{already} connected to it and pores farther away than the \emph{maximal throat length} $L_m$. It is also ensured that the matched pores are unique\footnote{There is a minor edge-case here that applies only if the generated network is of the \textbf{same size} as the original in any one direction. The issue is that both the original pore as well as one of its periodic copies are part of the same neighborhood. However, this is handled accordingly.}. If at any point no possible matches are found, then the corresponding entry in the pore-match table is marked as \emph{invalid}.
	\end{multicols}
	
	\begin{figure}[b!]
		\centering
		\begin{tikzpicture}
			\draw[black] (-3,-3) grid (3,3);
			\draw[draw=orange, fill=orange!10!white] (-3, -3) grid (2, -2) rectangle (-3, -3);
			\draw[draw=orange, fill=orange!10!white] (2, -3) grid (3, 3) rectangle (2, -3);
			\draw[draw=orange, fill=orange!10!white] (-3, 2) grid (2, 3) rectangle (-3, 2);
			\draw[draw=orange, fill=orange!10!white] (-3, -2) grid (-2, 2) rectangle (-3, -2);
			\draw[draw=blue, fill=blue!10!white] (-2, -1) grid (1, 2) rectangle (-2, -1);
			\node[anchor=north, black] at (-2.5, -3) {$0$};
			\node[anchor=north, black] at (-1.5, -3) {$1$};
			\node[anchor=north, black] at (-0.5, -3) {$2$};
			\node[anchor=north, black] at (0.5, -3) {$3$};
			\node[anchor=north, black] at (1.5, -3) {$4$};
			\node[anchor=north, black] at (2.5, -3) {$5$};
			
			\node[anchor=east, black] at (-3, -2.5) {$0$};
			\node[anchor=east, black] at (-3, -1.5) {$1$};
			\node[anchor=east, black] at (-3, -0.5) {$2$};
			\node[anchor=east, black] at (-3, 0.5) {$3$};
			\node[anchor=east, black] at (-3, 1.5) {$4$};
			\node[anchor=east, black] at (-3, 2.5) {$5$};
			% Points
			\node[red] at (-0.5, 0.5) {\textbullet};
			\node[black] at (-1.04, 1.78) {\textbullet};
			\node[black] at (2.96, 1.78) {\textbullet};
			\node[black] at (-1.04, -2.22) {\textbullet};
			\node[black] at (2.96, -2.22) {\textbullet};
			\node[black] at (-0.10, 0.20) {\textbullet};
			\node[black] at (-0.13, -0.03) {\textbullet};
			\node[black] at (0.44, 0.23) {\textbullet};
			\node[black] at (-0.02, 1.70) {\textbullet};
			\node[black] at (-0.02, -2.30) {\textbullet};
			\node[black] at (-1.33, 1.17) {\textbullet};
			\node[black] at (2.67, 1.17) {\textbullet};
			\node[black] at (-1.33, -2.83) {\textbullet};
			\node[black] at (2.67, -2.83) {\textbullet};
			\node[black] at (0.63, -0.33) {\textbullet};
			\node[black] at (-1.05, 1.71) {\textbullet};
			\node[black] at (2.95, 1.71) {\textbullet};
			\node[black] at (-1.05, -2.29) {\textbullet};
			\node[black] at (2.95, -2.29) {\textbullet};
			\node[black] at (-1.33, 0.53) {\textbullet};
			\node[black] at (2.67, 0.53) {\textbullet};
			\node[black] at (-0.86, 0.14) {\textbullet};
			\node[black] at (-1.43, -0.91) {\textbullet};
			\node[black] at (2.57, -0.91) {\textbullet};
			\node[black] at (0.20, 1.58) {\textbullet};
			\node[black] at (0.20, -2.42) {\textbullet};
			\node[black] at (1.08, 1.53) {\textbullet};
			\node[black] at (-2.92, 1.53) {\textbullet};
			\node[black] at (1.08, -2.47) {\textbullet};
			\node[black] at (-2.92, -2.47) {\textbullet};
			\node[black] at (-0.83, -1.07) {\textbullet};
			\node[black] at (-0.83, 2.93) {\textbullet};
			\node[black] at (1.51, -0.19) {\textbullet};
			\node[black] at (-2.49, -0.19) {\textbullet};
			\node[black] at (1.25, 0.25) {\textbullet};
			\node[black] at (-2.75, 0.25) {\textbullet};
			\node[black] at (0.24, -1.89) {\textbullet};
			\node[black] at (0.24, 2.11) {\textbullet};
			\node[black] at (1.64, -1.59) {\textbullet};
			\node[black] at (-2.36, -1.59) {\textbullet};
			\node[black] at (1.64, 2.41) {\textbullet};
			\node[black] at (-2.36, 2.41) {\textbullet};
			\node[black] at (0.81, -0.14) {\textbullet};
			\node[black] at (0.99, 1.11) {\textbullet};
			\node[black] at (0.99, -2.89) {\textbullet};
			\node[black] at (1.56, 0.19) {\textbullet};
			\node[black] at (-2.44, 0.19) {\textbullet};
			\node[black] at (0.00, 0.29) {\textbullet};
			\node[black] at (-0.59, 1.98) {\textbullet};
			\node[black] at (-0.59, -2.02) {\textbullet};
			\node[black] at (0.69, 0.22) {\textbullet};
			\node[black] at (1.72, -0.51) {\textbullet};
			\node[black] at (-2.28, -0.51) {\textbullet};
			\node[black] at (1.46, 0.32) {\textbullet};
			\node[black] at (-2.54, 0.32) {\textbullet};
			\node[black] at (-1.95, -1.76) {\textbullet};
			\node[black] at (2.05, -1.76) {\textbullet};
			\node[black] at (-1.95, 2.24) {\textbullet};
			\node[black] at (2.05, 2.24) {\textbullet};
			\node[black] at (-1.17, 0.31) {\textbullet};
			\node[black] at (2.83, 0.31) {\textbullet};
			\node[black] at (0.55, -0.60) {\textbullet};
			\node[black] at (0.38, -1.83) {\textbullet};
			\node[black] at (0.38, 2.17) {\textbullet};
			\node[black] at (-0.87, 1.61) {\textbullet};
			\node[black] at (-0.87, -2.39) {\textbullet};
			\node[black] at (-0.72, -1.95) {\textbullet};
			\node[black] at (-0.72, 2.05) {\textbullet};
			\node[black] at (0.23, -0.43) {\textbullet};
			\node[black] at (-1.72, 1.11) {\textbullet};
			\node[black] at (2.28, 1.11) {\textbullet};
			\node[black] at (-1.72, -2.89) {\textbullet};
			\node[black] at (2.28, -2.89) {\textbullet};
			\node[black] at (-0.03, -0.16) {\textbullet};
			\node[black] at (1.77, 1.06) {\textbullet};
			\node[black] at (-2.23, 1.06) {\textbullet};
			\node[black] at (1.77, -2.94) {\textbullet};
			\node[black] at (-2.23, -2.94) {\textbullet};
			\node[black] at (-0.79, 0.18) {\textbullet};
			\node[black] at (-1.34, -0.35) {\textbullet};
			\node[black] at (2.66, -0.35) {\textbullet};
			\node[black] at (-1.77, -1.49) {\textbullet};
			\node[black] at (2.23, -1.49) {\textbullet};
			\node[black] at (-1.77, 2.51) {\textbullet};
			\node[black] at (2.23, 2.51) {\textbullet};
			\node[black] at (-0.13, -0.43) {\textbullet};
			\node[black] at (-0.28, 0.22) {\textbullet};
			\node[black] at (1.69, 0.74) {\textbullet};
			\node[black] at (-2.31, 0.74) {\textbullet};
			\node[black] at (0.16, -0.40) {\textbullet};
			\node[black] at (1.73, -1.17) {\textbullet};
			\node[black] at (-2.27, -1.17) {\textbullet};
			\node[black] at (1.73, 2.83) {\textbullet};
			\node[black] at (-2.27, 2.83) {\textbullet};
		\end{tikzpicture}
		\caption{Cell lists visualized in 2D. The neighborhood of the pore highlighted in red is marked in blue. Because the cell-size is the maximally permissible throat length $L_m$, only the pores contained within the blue region must be considered during neighbor search. Finally, the periodic buffer layers, containing copies of pores on the opposite side from the interior, are painted in orange.}
		\label{fig:cell}
	\end{figure}	
	
	\newpage
	
	\begin{multicols}{2}
		\subsection{Iterative Algorithm}
		\hspace{0.5cm}To connect the generated pores by throats, we employ an \textbf{iterative} strategy. Beforehand however, the cell list is constructed based on the extent of the full domain (including periodic buffer layer) and cell-size $L_m$. Next, all pores are placed in their respective cell computed from their position. Now, for each pore and for each of its throats, which are copied from the base network and are sought to be realized, we find an \emph{ideal} match from its \textbf{neighboring cells}. Here, ideal refers to minimal absolute difference between physical distance of the pores and original length of the current throat $L_t$. Given position of $i$-th pore $\mathbf{p}_i$ and original throat length $L_t$ we seek:
		\begin{equation}
			j^* = \argmin_{j \in \mathcal{N}(i)}\Bigr| ||\mathbf{p}_i - \mathbf{p}_j||_2 - L_t \Bigr|
		\end{equation}
		Where $\mathcal{N}(i)$ is the set of all pores in adjacent cells to pore $i$. Finding $j^*$ for different pores is \emph{embarrassingly parallel} and can therefore be computed by a large number of threads, storing their results in shared memory. Each thread works on an even chunk of the pores located \emph{inside} the domain. Subsequently, these ideal matches are connected by throats, while avoiding \textbf{conflicts} of pores seeking either a neighbor that is already fully-connected or that was previously connected to them. Finally we repeat the two steps from above, now only considering pores that still have throats left, in \emph{random} order (for improved \textbf{load balancing}), until \textbf{no more valid matches} can be found. Empirically, this process converges very fast and consistently, for different target sizes of the full domain, after merely 7-8 iterations. The first iteration alone leaves only $\approx 18\%$ of all possible throats unrealized, see Table ~\ref{table:iter}. Final iterations may be performed serially if only few pores remain as to avoid costly spawning of threads without speed gain. The procedure is summarized as pseudo-code in Algorithm ~\ref{alg:connect}.
	\end{multicols}

	\begin{table}[b]
		\vspace{-0.5cm}
		\centering
		\caption{Sample run of parallel pore-connecting algorithm using 4 threads. The generated network is 3 times as large as the base network in all directions. The maximal feasible number of throats is 103464, of which 27 were not realized due to there being no possible candidates left for these remaining pores.}
		\begin{tabular}{|c|c|c|}
			\hline
			\textbf{Iteration} & \textbf{Throats unrealized} & \textbf{Rel. percentage} \\
			\hline
			0 & 103464 & 100.0\% \\
			1 & 18690 & 18.1\% \\
			2 & 4329 & 4.2\% \\
			3 & 1096 & 1.1\% \\
			4 & 282 & 0.3\% \\
			5 & 73 & 0.1\% \\
			6 & 30 & 0.03\% \\
			7 & 27 & 0.026\% \\
			\hline
		\end{tabular}
		\label{table:iter}
	\end{table}

	\newpage
	
	\begin{algorithm}[H]
		\caption{Connect pores in parallel}
		\begin{algorithmic}
			\State Initialize $cellList$ using $pores$ and compute $totalThroats$
			\State $poresRemain \gets pores$
			\State $throatsLeft \gets totalThroats$
			\State $throatsUnrealized \gets totalThroats$
			\While{$throatsLeft > 0$}
			\State Spawn $nthreads$ threads
			\State Compute best matches for all pores in $poresRemain$ using threads
			\State Store result in shared memory location $poreMatchTable$
			\For{$pore$ \textbf{in} $poresRemain$}
			\For{$throat$ \textbf{in} $pore.throats$}
			\State Fetch $match$ pore from $poreMatchTable$
			\If{$throat$ is not already realized \textbf{and} $match$ is valid}
			\State Realize $throat$
			\EndIf
			\EndFor
			\EndFor
			\State Compute list of pores with throats left in random order: $nextPores$
			\State $poresRemain \gets nextPores$
			\State Count remaining $throats$ having valid matches: $nextThroats$
			\State $throatsLeft \gets nextThroats$
			\State Count $throats$ that have been successfully realized: $realizedThroats$
			\State $throatsUnrealized \gets totalThroats - realizedThroats$
			\EndWhile
		\end{algorithmic}
		\label{alg:connect}
	\end{algorithm}
	
	\begin{multicols}{2}
		\subsection{Results}
		\hspace{0.5cm}Lastly, we discuss the quality of network realizations generated by our method. The immediate advantage of this iterative algorithm is that it connects as many pores as possible, achieving a much \textbf{larger fraction of realized throats} in the process. Apart from this, we take a closer look at the established throat connections, by examining their \textbf{deviations} from the target lengths $L_t$ of the original network. In the following, we focus on the average deviation for all realized throats and normalize it using the maximal throat length $L_m$. This way, we further compare the quality of networks generated by the serial and parallel algorithms respectively. For such networks of increasing size, we list the mentioned quality measures for both implementations in Table ~\ref{table:comp}. It is apparent that our method not only realizes many more throat connections, but they are also closer to the imposed target length \emph{on average}.
	\end{multicols}

	\begin{table}[ht]
		\centering
		\caption{Shown are the fraction of throats that were left unrealized, as well as the average deviation from the target length relative to $L_m$, for both serial and parallel algorithms applied to networks of increasing size.}
		\begin{tabular}{|p{0.15\textwidth}|p{0.15\textwidth}|p{0.15\textwidth}|p{0.15\textwidth}|p{0.15\textwidth}|}
			\hline
			\textbf{\#Throats} & \textbf{Unrealized \textcolor{blue}{parallel}} & \textbf{Unrealized \textcolor{red}{serial}} & \textbf{Deviation \textcolor{blue}{parallel}} & \textbf{Deviation \textcolor{red}{serial}} \\
			\hline
			3832 & 0.05\% & 0.26\% & 1.59\% & 2.83\% \\
			7664 & 0.04\% & 0.26\% & 1.50\% & 2.72\% \\
			15328 & 0.03\% & 0.39\% & 1.55\% & 2.73\% \\
			30656 & 0.04\% & 0.36\% & 1.59\% & 2.76\% \\
			\hline
		\end{tabular}
		\label{table:comp}
	\end{table}
	\newpage
	
	\begin{multicols}{2}
		\section{Performance Analysis}
		\label{sec:perf}
		\hspace{0.5cm}In the following we discuss the performance of our parallel pore-connecting algorithm and compare it to the existing serial implementation from \cite{MEYER2021101592}. Particularly, we are interested in the \textbf{scaling} of our method with respect to the number of processes and the difference in \textbf{run-time} between the two implementations for varying target sizes of the generated domain. All performance measurements were conducted on \emph{Euler} and the specific type of node used is mentioned together with the results. The last sub-section is then dedicated to the distributed flow solver, comparing it to its serial counterpart for an increasing number of pores.
		\subsection{Strong Scaling}
		\hspace{0.5cm}Firstly, we investigate the \textbf{strong scaling} as shown in Figure ~\ref{fig:strong}. We observe that our method scales fairly well for an increasing number of processes, as the measurements lie reasonably close to the \emph{ideal} case. As expected, the speedup is not perfect, since the network generation function is not fully parallelized. In particular, the distribution of pores using dendrogram-based clustering is still serial. Additionally, connecting the individual pores cannot be parallelized in a useful way, as it involves substantial complexity concerning synchronization. It is also significantly faster and computationally cheap, than the parallel match finding. Despite all this, because these serial parts only account for a small fraction of the total computation time, we still observe a large \textbf{speedup} of up to a factor $20$.
		\subsection{Comparison}
		\hspace{0.5cm}Secondly, we compare the performance of the concurrent algorithm with that of the serial version. To do this, we consider increasing target sizes, starting from the original size of the network. Next, we generate networks that are twice as large in the first, then also the second spatial direction etc. We continue this process of doubling individual dimension sizes until we reach a network that is 4 times as large in every direction, or in total 64 times as large as the original base network. The results, which are depicted in Figure ~\ref{fig:comp}, indicate a drastic improvement in terms of computation time over the original implementation.
	\end{multicols}
	
	\begin{figure}[ht!]
		\centering
		\caption{Performance analysis for parallel network generation algorithm.}
		\begin{subfigure}[t]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{plots/strong_scaling.png}
		\caption{For a fixed problem size where the network to be generated is $3^3 = 27$ times as large as the base network, we compute the \emph{speedup} for $1, 2, 4, 8, 16$ and $32$ threads. The time measurements of the parallel generation algorithm were conducted on an Euler IV node equipped with Intel Xeon Gold 6150 CPUs and $36$ cores in total.}
		\label{fig:strong}
		\end{subfigure}
		\hfill
		\begin{subfigure}[t]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{plots/comparison_time.png}
		\caption{Run-times of both serial and parallel algorithms in seconds, as measured on Euler IV node. Measurements are only performed once, since the impact of noise is negligible for sufficiently large networks. The generated size on the $x$-axis refers to the number of times the base network fits inside the generated network. The parallel version is consistently between $10$ and $20$ times faster.}
		\label{fig:comp}
		\end{subfigure}
	\end{figure}
	
	\begin{multicols}{2}
		\subsection{Flow Simulation}
		\hspace{0.5cm}In the next step we evaluate the performance of our distributed flow solver as introduced in section ~\ref{sec:solver}. Firstly, we consider the impact of the number of processes used in the iterative solver. Next, we compare the existing, serial algebraic multi-grid solver to our method based on a distributed version supplied by hypre \cite{hypre-web-page}. As discussed earlier, the communication necessary for processes to have a consistent view of the linear system affects the performance. This is especially true for a large number of processes mapped to \emph{different nodes}, requiring the slower inter-node communication to take place. From Figure ~\ref{fig:distr}, it is evident that larger number of ranks incur \emph{diminishing returns} w.r.t. to speedup of the solver, which is a consequence of \textbf{communication overhead}. Lastly, we directly compare the run-times of both methods, applied to periodic network realizations of increasing size, see Figure ~\ref{fig:cmpsolve}. We observe a consistent speedup between a factor of $5$ and $10$ over the serial version, while maintaining the overall quality of the pressure solution, as already established in section ~\ref{sec:quality}.
	\end{multicols}

	\newpage
	
	\begin{figure}[ht!]
		\vspace{-1.0cm}
		\centering
		\caption{Performance analysis for distributed, iterative flow solver based on PETSc \cite{petsc-web-page}. We simulate \emph{periodic} network flow with a pressure gradient in $z$-direction of $100 \;\mathrm{Pa}\;\mathrm{m}^{-1}$ and dynamic viscosity $\mu = 10^{-3} \;\mathrm{Pa}\;\mathrm{s}$.}
		\begin{subfigure}[t]{0.45\textwidth}
			\centering
			\includegraphics[width=\textwidth]{plots/scaling_distributed.png}
			\caption{For a fixed periodic network comprised of $46386$ pores and $103412$ throats, we simulate the flow using our distributed algebraic multi-grid (AMG) solver. Shown is the speedup measured on an Euler IV node for communicator sizes $1, 2, 4, 8, 16$ and $32$.}
			\label{fig:distr}
		\end{subfigure}
		\hfill
		\begin{subfigure}[t]{0.45\textwidth}
			\centering
			\includegraphics[width=\textwidth]{plots/comparison_flow.png}
			\caption{Average run-time in seconds of $4$ consecutive runs for both serial and distributed AMG flow solvers, as measured on Euler IV node with our method using 36 ranks. Both methods are applied to increasingly large periodic networks generated using our parallel algorithm.}
			\label{fig:cmpsolve}
		\end{subfigure}
	\end{figure}
	
	\begin{multicols}{2}
		\section{Discussion}
		\hspace{0.5cm}Throughout this work we have achieved sizeable performance improvements over the previous, serial implementations from \cite{MEYER2021101592}, especially regarding the generation algorithm. This allows us to compute much larger network realizations of improved quality and simulate the flow through them within a reasonable time-frame of a few hours. The largest network generated using our method has a volume of $1.93\times 1.93\times 1.93 \;\mathrm{cm}^3$ and contains just over \textbf{10 million pores}. Generation of this network took about 5 hours of CPU time on an Euler V node using 24 threads. For larger network realizations however, memory management while using many threads becomes challenging, as the data types and structures in place take up a lot of space. To enable \emph{efficient} generation of networks larger than that, this issue would have to be addressed in due time. As for future work, it would be desirable to allow network generation followed directly by flow simulation through the resulting network, both in parallel, bypassing the file-system in the process. This is possible with our code, however the \textbf{MPI-based solver} and \textbf{multi-threaded generator} require \emph{splitting up} the available CPU cores. Using MPI for generation also, could avoid this. Additionally, of great interest is the study of \emph{fluid particle transport} through increasingly large, periodic network samples, generated using our method.
		\section{Acknowledgments}
		\hspace{0.5cm}The author thankfully acknowledges the various discussions held with supervisor Daniel W. Meyer, who has guided the course of this thesis and offered valuable insight during development of the code.
	\end{multicols}

	\newpage
		
	\appendix
	\begin{center} \Large{\textbf{Appendix}} \end{center}
	\section{PETSc Installation}
	\label{appendix:install}
	For the purposes of this thesis, PETSc was installed in the following way. Given \textbf{existing} (open)MPI compilers located at \verb|/usr/bin/| (Linux).
	\begin{itemize}
		\item Clone \href{https://gitlab.com/petsc/petsc}{PETSc repository} from \cite{petsc-web-page}.
		\item Run \textbf{configuration} script:\\
		\begin{tabular}{ l l }
			\verb|./configure| & \verb|--with-cc=/usr/bin/mpicc| \\
							   & \verb|--with-cxx=/usr/bin/mpicxx| \\
							   & \verb|--with-fc=0| \\
							   & \verb|--download-hypre|
		\end{tabular}
		\item Set environment flags \verb|PETSC_DIR| and \verb|PETSC_ARCH| as specified in the output of \verb|./configure| \& run \textbf{make all}.
	\end{itemize}
	
	\section{Base Network}
	\label{appendix:base}
	Throughout this thesis we rely on existing networks obtained via tomographic scans to serve as a \emph{basis} for \textbf{generation} of larger networks and \textbf{simulation} of network flow. The statistics of the base network mentioned previously and used in code are detailed in Figure ~\ref{fig:base}.
	
	\begin{figure}[ht!]
		\vspace{-0.3cm}
		\centering
		\includegraphics[width=0.6\textwidth]{plots/base.png}
		\caption{Network consisting of 2636 pores and 4291 throats, inscribed within a cube extending $1.07$ mm in each spatial direction. The pore-arrangement is obviously \emph{not uniform}, as can be seen by the clustering of pores in some regions, while others are mostly undisturbed. The \emph{porosity}, measured as the relative fraction of volume taken up by the void-space geometry, is roughly $32\%$.}
		\label{fig:base}
	\end{figure}
	
	\newpage
	
	\centering
	\printbibliography
	
	\newpage
	
	\includepdf{declaration/Declaration.pdf}
	
\end{document}