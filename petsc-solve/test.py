# Solve pressure system (in/out) for base network using PETSc and others.
# Derived fluxes are computed and plotted for all solvers.
#
# Severin Fritschi
# December 2021

from mpi4py import MPI

import sys
sys.path.append('../')
sys.path.append('../netflow/')

from netflow import *
from math import sqrt

import numpy as np
import matplotlib.pyplot as plt

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
root = 0

def plot_flux(inpores, outpores, p, Q, solver_name):
    Qb = netflow.flux_balance(p, Q)
    
    # Exclude in- and out-pores respectively
    Qb = {pore: Qb[pore] for pore in Qb if (pore not in inpores) and (pore not in outpores)}
    x  = np.arange(len(Qb))
    
    Qp = np.array( [Qb[k][0] for k in Qb] )
    Qm = np.array( [Qb[k][1] for k in Qb] )
    Qs = np.array( [Qb[k][2] for k in Qb] )
    
    plt.figure()
    plt.title(r"Pore Flux Statistics for Solver: {}".format(solver_name))
    plt.xlabel(r"Pore Index (no in-/out-pores)")
    plt.ylabel(r"Flux [$m^3s^{-1}$]")
    plt.plot(x, Qp, label=r"out-going")
    plt.plot(x, Qm, label=r"in-coming")
    plt.plot(x, Qs, label=r"sum", linewidth=2)
    plt.legend()
    plt.savefig("plots/flux_{}.png".format(solver_name))
    plt.close()

def main():    
    basenet = None
    inpores  = set()
    outpores = set()
    
    # Initialization on root only
    if (rank == root):
        basenet = netflow.load_network_from('../network/network.h5')
        for pore in basenet.pores:
            if pore.label[:2] == 'in':
                inpores.add(pore)
            elif pore.label[:3] == 'out':
                outpores.add(pore)
        
        print("Pores: %d In-pores: %d Out-pores: %d" % (len(basenet.pores), \
                                                          len(inpores), \
                                                            len(outpores)))
    
    # Compute solution for each solver and compare flux balances
    for solver in netflow.Solver:
        start = MPI.Wtime()
        p, Q = netflow.solve_flow_inout(network=basenet, pin=1e4, \
                   pout=0.0, inpores=inpores, outpores=outpores, \
                       solver=solver, mu=1e-3)
        end = MPI.Wtime()
        
        if rank == root:
            print("Solver: %s Time: %e s" % (solver.name, end - start))
            
            plot_flux(inpores, outpores, p, Q, solver.name)
        comm.Barrier()
	
if __name__ == '__main__':
    main()
