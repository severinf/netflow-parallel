# Plot scaling of parallel flow solver from existing measurements
#
# Severin Fritschi
# December 2021

import numpy as np
import matplotlib.pyplot as plt

if __name__ == '__main__':
    data = np.loadtxt('measurements.txt')
    procs = data[:, 0]
    times = data[:, 1]
    n = procs.shape[0]
    
    speedups = np.array([times[n-1] / times[i] for i in range(n)])
        
    plt.figure()
    plt.title(r"Strong Scaling Distributed Flow Solver")
    plt.xlabel(r"#ranks")
    plt.ylabel(r"Speedup")
    plt.plot(procs, speedups, '--bo')
    plt.grid(True)
    plt.savefig('../plots/scaling_distributed.png')
    plt.close()
