#!/bin/bash
for i in {1,2,4,8,16,32}
do
    echo "Using $i processes";
    mpirun -n $i python3 solve_flow.py $1;
done
