# Solve flow for given number of ranks and write run-times to
# measurements.txt.
#
# User specifies:
# - Network to be used for simulation (must be periodic)
#
# Severin Fritschi
# December 2021

from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()
root = 0

import sys
sys.path.append('../../')
sys.path.append('../../netflow/')

import netflow.netflow as netflow_par
import netflow.netgen as netgen_par

def main():
    
    if (len(sys.argv) != 2):
        if (rank == root):
            print("Usage: python3 solve_flow.py <path-to-network>")
        comm.Abort(1)
    
    net = None
    f = None
    # Only initialize on root rank
    if (rank == root):
        f = open('measurements.txt', 'a+')
        netPath = sys.argv[1]
        try:
            net = netgen_par.load_network_from(netPath)
        except Exception as e:
            print("Error: {}".format(e))
            comm.Abort(1)
        
        print("Pores: %d" % (len(net.pores)))
    
    kreps = 4
    
    elapsedPetsc = 0.
    for _ in range(kreps):
        start = MPI.Wtime()
        pA, QA = netflow_par.solve_flow_periodic(network=net, \
                    solver=netflow_par.Solver.PETSC, mu=1e-3, c=3, P2L=1e2)
        end = MPI.Wtime()
        elapsedPetsc += end - start
    
    elapsedPetsc /= kreps
    
    if rank == root:
        f.write("%d\t%e\n" % (size, elapsedPetsc))
        f.close()
    
if __name__ == '__main__':
    main()
