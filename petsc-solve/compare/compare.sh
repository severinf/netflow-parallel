#!/bin/bash
# Target Euler IV nodes with 36 cores
bsub -n 36 -R fullnode -W 00:30 mpirun -n 36 python3 compare_flow.py 2
