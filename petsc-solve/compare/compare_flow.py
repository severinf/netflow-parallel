# Using previously generated networks, compare runtimes of
# PETSc solver and AMG solver from PyAMG.
#
# User specifies:
# - Max. power of 2 of original network size; must be <= 2
#
# Severin Fritschi
# December 2021

from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()
root = 0

import sys
sys.path.append('../../')
sys.path.append('../../netflow/')

import matplotlib.pyplot as plt

import netflow.netflow as netflow_par
import netflow.netgen as netgen_par
import netflow_serial.netflow as netflow_ser

def compute_target_sizes(max_power2):
    power = 0
    d = 3
    target_sizes = [[1, 1, 1]]
    while (power < max_power2):
        prev = target_sizes[d * power]
        for i in range(d):
            t = prev.copy()
            t[i] <<= 1
            prev = t
            
            target_sizes.append(t)
        power += 1
    return target_sizes
    
def main():
    
    if (len(sys.argv) != 2):
        if (rank == root):
            print("Usage: python3 compare_flow.py <max_power>")
        comm.Abort(1)
    
    net = None
    kreps = 4
    
    targetsizes = compute_target_sizes(int(sys.argv[1]))
    
    sizes = []
    timesPetsc = []
    timesPyamg = []
    for target in targetsizes:
        sizes.append(netgen_par.prod(target))
        # Only initialize on root rank
        if (rank == root):
            netPath = "../../gen/networks/dendro_{}_{}_{}.h5".format(*target)
            try:
                net = netgen_par.load_network_from(netPath)
            except Exception as e:
                print("Error: {}".format(e))
                comm.Abort(1)
            
            print("Pores: %d" % (len(net.pores)))
                
        elapsedPetsc = 0.
        for _ in range(kreps):
            start = MPI.Wtime()
            pA, QA = netflow_par.solve_flow_periodic(network=net, \
                        solver=netflow_par.Solver.PETSC, mu=1e-3, c=3, P2L=1e2)
            end = MPI.Wtime()
            elapsedPetsc += end - start
        
        elapsedPetsc /= kreps
        timesPetsc.append(elapsedPetsc)
        
        if (rank == root):
            print("PETSc time: %e s" % elapsedPetsc)
        
        if rank == root:
            elapsedOther = 0.
            for _ in range(kreps):
                start = MPI.Wtime()
                pB, QB = netflow_ser.solve_flow_periodic(network=net, mu=1e-3, c=3, P2L=1e2)
                end = MPI.Wtime()
                elapsedOther += end - start
            
            elapsedOther /= kreps
            timesPyamg.append(elapsedOther)
            
            print("PyAMG time: %e s" % elapsedOther)
            print("Speedup: %.2f" % (elapsedOther / elapsedPetsc))
            print("")
    
    if (rank == root):
        plt.figure()
        plt.title(r"Comparison Between Parallel and Serial AMG Solvers")
        plt.xlabel(r"Multiplicative generated size")
        plt.ylabel(r"Time [s]")
        plt.plot(sizes, timesPyamg, '--rx', label="Serial")
        plt.plot(sizes, timesPetsc, '--bo', label="PETSc using {} ranks".format(size))
        plt.legend()
        plt.grid(True)
        plt.savefig('../plots/comparison_flow.png')
        plt.close()
    
if __name__ == '__main__':
    main()
