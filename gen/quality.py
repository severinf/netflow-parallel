# Serial and parallel network generation for increasing
# network sizes (powers of 2 or original).
# Used to compare quality w.r.t. average throat length deviation
# and fraction of unrealized throats.
#
# User specifies:
# - Number of threads used
# - Max. power of 2 of original network size
#
# Severin Fritschi
# December 2021

import sys
sys.path.append('../')

import netflow.netgen as netgen_par
import netflow_serial.netgen as netgen_ser

def compute_target_sizes(max_power2):
    power = 0
    d = 3
    target_sizes = [[1, 1, 1]]
    while (power < max_power2):
        prev = target_sizes[d * power]
        for i in range(d):
            t = prev.copy()
            t[i] <<= 1
            prev = t
            
            target_sizes.append(t)
        power += 1
    return target_sizes
        
def main():
    if (len(sys.argv) != 3):
        raise AssertionError("Usage: python3 quality.py <nthreads> <max_power>")
    basenet = netgen_par.load_network_from('../network/network.h5')
    print("Network statistics:")
    print("Throats: %d" % len(basenet.throats))
    pore_throat_counts = [len(pore.throats) for pore in basenet.pores]
    print("Max. number of throats per pore: %d" % max(pore_throat_counts))
    print("Min. number of throats per pore: %d" % min(pore_throat_counts))
    print("Avg. number of throats per pore: %f" % (sum(pore_throat_counts) / len(pore_throat_counts)))
    
    del pore_throat_counts
    
    nthreads = int(sys.argv[1])
    print("Using: {} threads".format(nthreads))
    targetsizes = compute_target_sizes(int(sys.argv[2]))
    
    cutoff  = 0.5 * max([basenet.ub[i] - basenet.lb[i] \
                        for i in range(len(basenet.ub))])
    
    for target in targetsizes:
        print("Target size: {}".format(target))
        
        _ = netgen_par.generate_dendrogram(basenet=basenet, targetsize=target, \
                                        cutoff=cutoff, sd=42, nthreads=nthreads, mute=False)
        _ = netgen_ser.generate_dendrogram(basenet=basenet, targetsize=target, \
                                        cutoff=cutoff, sd=42, mute=False)
        
    
if __name__ == '__main__':
    main()
