# Parallel, dendrogram-based network generation script.
#
# User specifies:
# - Number of threads used
# - Target dimensions [n1,n2,n3] of network to be generated
# - Whether or not (y/n) generated network shall be saved in networks/
#
# Severin Fritschi
# December 2021

import sys
sys.path.append('../')

from netflow import *

from time import perf_counter
        
def main():
    if len(sys.argv) != 6:
        raise AssertionError("Usage: python3 generate.py <nthreads> <n1> <n2> <n3> <save network?: y/n>")
    nthreads = int(sys.argv[1])
    
    print("Using %d thread(s)" % nthreads)
    basenet = netflow.load_network_from('../network/network.h5')
    print("Network statistics:")
    pore_throat_counts = [len(pore.throats) for pore in basenet.pores]
    print("Max. number of throats per pore: %d" % max(pore_throat_counts))
    print("Min. number of throats per pore: %d" % min(pore_throat_counts))
    print("Avg. number of throats per pore: %f" % (sum(pore_throat_counts) / len(pore_throat_counts)))
    
    del pore_throat_counts
    
    target  = [int(sys.argv[2]), int(sys.argv[3]), int(sys.argv[4])]
    print("Target size: {}".format(target))
    cutoff  = 0.5 * max([basenet.ub[i] - basenet.lb[i] \
                        for i in range(len(basenet.ub))])
    
    start = perf_counter()
    dendro = netflow.generate_dendrogram(basenet=basenet, targetsize=target, \
                                        cutoff=cutoff, sd=42, nthreads=nthreads, \
                                        mute=False)
    end = perf_counter()
    elapsed = end - start
    print("Elapsed time: %e s" % elapsed)
    
    """
    # Plot network (optional)
    
    import matplotlib.pyplot as plt
    _ = netgen.plot_network(dendro)
    plt.savefig('plots/dendro.png')
    """
    if (sys.argv[5].lower() == 'y'):
        path = 'networks/dendro_{}_{}_{}.h5'.format(*target)
        netflow.save_network_to(path, dendro)
        print("network saved to: {}".format(path))
        
    print("\nGenerated network statistics:")
    print("Throats: %d" % len(dendro.throats))
    print("Pores:   %d" % len(dendro.pores))
    
if __name__ == '__main__':
    main()
