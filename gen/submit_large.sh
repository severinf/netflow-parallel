#!/bin/bash
# Target Euler nodes with 24 cores
bsub -n 24 -W 06:00 -R fullnode -R "rusage[mem=5300]" python3 generate.py 24 18 18 18 y
