#!/bin/bash
# Targets Xeon Gold 6150 nodes with 36 cores/node
bsub -n 36 -W 04:00 -R fullnode python3 compare.py 36 2
