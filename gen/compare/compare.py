# Serial and parallel network generation for increasing
# network sizes (powers of 2).
# Runtimes are measured and plotted.
#
# User specifies:
# - Number of threads used
# - Max. power of 2 of generated networks
#
# Severin Fritschi
# December 2021

import sys
sys.path.append('../../')
import matplotlib.pyplot as plt

import netflow.netgen as netgen_par
import netflow_serial.netgen as netgen_ser

from time import perf_counter

def compute_target_sizes(max_power2):
    power = 0
    d = 3
    target_sizes = [[1, 1, 1]]
    while (power < max_power2):
        prev = target_sizes[d * power]
        for i in range(d):
            t = prev.copy()
            t[i] <<= 1
            prev = t
            
            target_sizes.append(t)
        power += 1
    return target_sizes
        
def main():
    if (len(sys.argv) != 3):
        raise AssertionError("Usage: python3 compare.py <nthreads> <max_power>")
    basenet = netgen_par.load_network_from('../../network/network.h5')
    print("Network statistics:")
    print("Throats: %d" % len(basenet.throats))
    pore_throat_counts = [len(pore.throats) for pore in basenet.pores]
    print("Max. number of throats per pore: %d" % max(pore_throat_counts))
    print("Min. number of throats per pore: %d" % min(pore_throat_counts))
    print("Avg. number of throats per pore: %f" % (sum(pore_throat_counts) / len(pore_throat_counts)))
    
    del pore_throat_counts
    
    nthreads = int(sys.argv[1])
    print("Using: {} threads".format(nthreads))
    targetsizes = compute_target_sizes(int(sys.argv[2]))
    
    cutoff  = 0.5 * max([basenet.ub[i] - basenet.lb[i] \
                        for i in range(len(basenet.ub))])
    
    speedups = []
    sizes = []
    times_par = []
    times_ser = []
    for i, target in enumerate(targetsizes):
        sizes.append(netgen_par.prod(target))
        print("Multiplicative generated size: {}".format(sizes[i]))
        
        start = perf_counter()
        dendro = netgen_par.generate_dendrogram(basenet=basenet, targetsize=target, \
                                        cutoff=cutoff, sd=42, nthreads=nthreads, mute=True)
        end = perf_counter()
        elapsed = end - start
        
        times_par.append(elapsed)
        
        print("Elapsed par: %e" % elapsed)
        print("Generated network statistics:")
        print("Throats: %d" % len(dendro.throats))
        print("Pores:   %d" % len(dendro.pores))
        
        start = perf_counter()
        dendro = netgen_ser.generate_dendrogram(basenet=basenet, targetsize=target, \
                                        cutoff=cutoff, sd=42, mute=True)
        end = perf_counter()
        elapsed = end - start
        
        times_ser.append(elapsed)
        
        speedups.append(times_ser[i] / times_par[i])
        
        print("\nElapsed ser: %e" % elapsed)
        print("Generated network statistics:")
        print("Throats: %d" % len(dendro.throats))
        print("Pores:   %d" % len(dendro.pores))
        
        
        print("")
        
    plt.figure()
    plt.title(r"Comparison Between Parallel and Serial Methods")
    plt.xlabel(r"Multiplicative generated size")
    plt.ylabel(r"Time [s]")
    plt.plot(sizes, times_ser, '--rx', label="Serial")
    plt.plot(sizes, times_par, '--bo', label="Parallel using {} threads".format(nthreads))
    plt.legend()
    plt.grid(True)
    plt.savefig('../plots/comparison_time.png')
    plt.close()
    
if __name__ == '__main__':
    main()
