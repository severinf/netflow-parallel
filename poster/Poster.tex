% This is a template for setting posters, and especially designed for seminar-/semester-/masterthesis posters at IFD (Institute of Fluid Dynamics, ETH Zurich).
% The current implementation sets a DIN-A4 poster.
% In order to set a DIN-A1 poster, comment and uncomment indicated lines of code.
%
% Author: Roman Fuchs, rofuchs@student.ethz.ch
% Date: June 2011
\documentclass{article}


%%% INPUT AND ENCODING
\usepackage[utf8]{inputenc} % support usage of umlaute (ä, ö, ü) into a LaTeX-specific coding.
\usepackage[T1]{fontenc} % support usage of umlaute from LaTeX-specific coding into a specific font style, here T1.


%%% POSTER DETAILS
\title{Generation and Simulation of Large-Scale Flow Networks}
\author{Severin Fritschi}
%\date{February 30th, 2099} 
\makeatletter
\newcommand{\@supervisor}{supervised by Daniel W. Meyer}
\makeatother


%%% PAGE DIMENSIONS
\usepackage[a4paper]{geometry} % to change the page dimensions
\geometry{left=2cm, right=2cm, top=2cm, bottom=2cm} 

%%% Page dimensions for DIN-A1 page size.
%\usepackage[a1paper]{geometry}
%\geometry{left=3.5cm, right=3.5cm, top=2.5cm, bottom=4cm}

%%% FONT SIZE for DIN A1 size 
% (attention: \Huge, \LARGE, \small, \footnotesize, \scriptsize, and \tiny are just copied values. You might adapt them!)
%\renewcommand{\Huge}{\fontsize{66}{80}\selectfont}
%\renewcommand{\huge}{\fontsize{66}{80}\selectfont}
%\renewcommand{\LARGE}{\fontsize{40}{48}\selectfont}
%\renewcommand{\Large}{\fontsize{40}{48}\selectfont}
%\renewcommand{\large}{\fontsize{36}{44}\selectfont}
%\renewcommand{\normalsize}{\fontsize{21}{26}\selectfont}
%\renewcommand{\small}{\fontsize{21}{26}\selectfont}
%\renewcommand{\footnotesize}{\fontsize{21}{26}\selectfont}
%\renewcommand{\scriptsize}{\fontsize{21}{26}\selectfont}
%\renewcommand{\tiny}{\fontsize{21}{26}\selectfont}



\setlength{\footskip}{15mm} %%% Maybe you have to adapt the distance between the footer and the text area, because of the ETH- and IFD logo.
%\addtolength{\textheight}{-20mm}


%%% GRAPHICS
\usepackage{graphicx} % support the \includegraphics command and options
\usepackage{tikz}

%%% TODO NOTES
\usepackage{todonotes} % provide annotations in the margin, \todo{Remeber!}, and dummy figures, \missingfigure{Dummy figure}.


%%% PARAGRAPH
\setlength{\parindent}{0pt} % no indent at new paragraphs, 
\setlength{\parskip}{0.5\baselineskip} % but a half line space between


%%% SANS SERIF FONT
\renewcommand{\familydefault}{\sfdefault} % Set sans serif style as default style
\usepackage{cmbright} % Set math formulas also in sans serif style


%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customise the layout...
\lhead{} % top left
\chead{} % top central
\rhead{} % top right
\lfoot{\includegraphics[height=1cm]{eth_logo_black.png}} % bottom left. Maybe use *.eps
\cfoot{} % bottom central
\rfoot{\includegraphics[height=1cm]{ifd_logo.pdf}} % bottom right. Maybe use *.eps


%%% MULTIPLE COLUMNS
\usepackage{multicol}
\setlength{\columnseprule}{1pt} % width of the line between the two columns. If you like no line, use 0pt (zero points)
\setlength{\columnsep}{20pt} % distance between columns and line


%%% OTHER PACKAGES
\usepackage{amsmath} % provide mathematical symbols and so
\usepackage{float} % Necessary to place figures and their caption correctly.



%%% DOCUMENT
\begin{document}


%%% TITLE (Don't change this!)
\begin{center}
	\rule{\linewidth}{2pt}
	\huge{\makeatletter\@title\makeatother} \\
	\Large{\makeatletter\@author\makeatother} \\
	\makeatletter\@supervisor\makeatother   \\
	%\makeatletter\@date\makeatother  \\
	\rule{\linewidth}{2pt}
\end{center}


\begin{multicols}{2} % Don't change. This command sets the following text in two columns, until \end{multicol}

%||%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%|| WRITE TEXT BELOW
%VV%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section*{Flow Networks}
In this work, we focus on \emph{flow networks} as a simplification of complex void-space geometries found in porous media, such as f.ex. soil or gravel. Spherical \textbf{pores} and cylindrical \textbf{throats}, connecting individual pore pairs, replace the 3D voxel-arrays obtained from tomographic scans. The resulting \emph{flow network} is visualized in Figure ~\ref{fig:basenet}.

\begin{figure}[H]
	\begin{center}
		\includegraphics[width=0.48\textwidth]{../presentation/figures/basenet.png}
	\end{center}
	\caption{3D rendering of base network.}
	\label{fig:basenet}
	\vspace{-1.5cm}
\end{figure}

\subsection*{Network Generation}
Given a base network, we seek to generate much larger network realizations in \textbf{parallel}. These networks can then be used to study the \emph{transport of particles} through e.g. soil as part of groundwater flow, taking place on an inherently larger scale than the comparatively small base network.


\subsection*{Cell Lists}
The most computationally expensive part of generating networks is connecting new pores with suitable neighbors. For this purpose we use the \emph{cell lists} data structure visualized for a 2D case in Figure ~\ref{fig:cell}. For each pore we only search within \textbf{adjacent cells} for potential neighbors, and finding them can be done easily in parallel.

\begin{figure}[H]
	\centering
	\begin{tikzpicture}[scale=0.8]
	\draw[black] (-3,-3) grid (3,3);
	\draw[draw=orange, fill=orange!10!white] (-3, -3) grid (2, -2) rectangle (-3, -3);
	\draw[draw=orange, fill=orange!10!white] (2, -3) grid (3, 3) rectangle (2, -3);
	\draw[draw=orange, fill=orange!10!white] (-3, 2) grid (2, 3) rectangle (-3, 2);
	\draw[draw=orange, fill=orange!10!white] (-3, -2) grid (-2, 2) rectangle (-3, -2);
	\draw[draw=blue, fill=blue!10!white] (-2, -1) grid (1, 2) rectangle (-2, -1);
	\node[anchor=north, black] at (-2.5, -3) {$0$};
	\node[anchor=north, black] at (-1.5, -3) {$1$};
	\node[anchor=north, black] at (-0.5, -3) {$2$};
	\node[anchor=north, black] at (0.5, -3) {$3$};
	\node[anchor=north, black] at (1.5, -3) {$4$};
	\node[anchor=north, black] at (2.5, -3) {$5$};
	
	\node[anchor=east, black] at (-3, -2.5) {$0$};
	\node[anchor=east, black] at (-3, -1.5) {$1$};
	\node[anchor=east, black] at (-3, -0.5) {$2$};
	\node[anchor=east, black] at (-3, 0.5) {$3$};
	\node[anchor=east, black] at (-3, 1.5) {$4$};
	\node[anchor=east, black] at (-3, 2.5) {$5$};
	% Points
	\node[red] at (-0.5, 0.5) {\textbullet};
	\node[black] at (-1.04, 1.78) {\textbullet};
	\node[black] at (2.96, 1.78) {\textbullet};
	\node[black] at (-1.04, -2.22) {\textbullet};
	\node[black] at (2.96, -2.22) {\textbullet};
	\node[black] at (-0.10, 0.20) {\textbullet};
	\node[black] at (-0.13, -0.03) {\textbullet};
	\node[black] at (0.44, 0.23) {\textbullet};
	\node[black] at (-0.02, 1.70) {\textbullet};
	\node[black] at (-0.02, -2.30) {\textbullet};
	\node[black] at (-1.33, 1.17) {\textbullet};
	\node[black] at (2.67, 1.17) {\textbullet};
	\node[black] at (-1.33, -2.83) {\textbullet};
	\node[black] at (2.67, -2.83) {\textbullet};
	\node[black] at (0.63, -0.33) {\textbullet};
	\node[black] at (-1.05, 1.71) {\textbullet};
	\node[black] at (2.95, 1.71) {\textbullet};
	\node[black] at (-1.05, -2.29) {\textbullet};
	\node[black] at (2.95, -2.29) {\textbullet};
	\node[black] at (-1.33, 0.53) {\textbullet};
	\node[black] at (2.67, 0.53) {\textbullet};
	\node[black] at (-0.86, 0.14) {\textbullet};
	\node[black] at (-1.43, -0.91) {\textbullet};
	\node[black] at (2.57, -0.91) {\textbullet};
	\node[black] at (0.20, 1.58) {\textbullet};
	\node[black] at (0.20, -2.42) {\textbullet};
	\node[black] at (1.08, 1.53) {\textbullet};
	\node[black] at (-2.92, 1.53) {\textbullet};
	\node[black] at (1.08, -2.47) {\textbullet};
	\node[black] at (-2.92, -2.47) {\textbullet};
	\node[black] at (-0.83, -1.07) {\textbullet};
	\node[black] at (-0.83, 2.93) {\textbullet};
	\node[black] at (1.51, -0.19) {\textbullet};
	\node[black] at (-2.49, -0.19) {\textbullet};
	\node[black] at (1.25, 0.25) {\textbullet};
	\node[black] at (-2.75, 0.25) {\textbullet};
	\node[black] at (0.24, -1.89) {\textbullet};
	\node[black] at (0.24, 2.11) {\textbullet};
	\node[black] at (1.64, -1.59) {\textbullet};
	\node[black] at (-2.36, -1.59) {\textbullet};
	\node[black] at (1.64, 2.41) {\textbullet};
	\node[black] at (-2.36, 2.41) {\textbullet};
	\node[black] at (0.81, -0.14) {\textbullet};
	\node[black] at (0.99, 1.11) {\textbullet};
	\node[black] at (0.99, -2.89) {\textbullet};
	\node[black] at (1.56, 0.19) {\textbullet};
	\node[black] at (-2.44, 0.19) {\textbullet};
	\node[black] at (0.00, 0.29) {\textbullet};
	\node[black] at (-0.59, 1.98) {\textbullet};
	\node[black] at (-0.59, -2.02) {\textbullet};
	\node[black] at (0.69, 0.22) {\textbullet};
	\node[black] at (1.72, -0.51) {\textbullet};
	\node[black] at (-2.28, -0.51) {\textbullet};
	\node[black] at (1.46, 0.32) {\textbullet};
	\node[black] at (-2.54, 0.32) {\textbullet};
	\node[black] at (-1.95, -1.76) {\textbullet};
	\node[black] at (2.05, -1.76) {\textbullet};
	\node[black] at (-1.95, 2.24) {\textbullet};
	\node[black] at (2.05, 2.24) {\textbullet};
	\node[black] at (-1.17, 0.31) {\textbullet};
	\node[black] at (2.83, 0.31) {\textbullet};
	\node[black] at (0.55, -0.60) {\textbullet};
	\node[black] at (0.38, -1.83) {\textbullet};
	\node[black] at (0.38, 2.17) {\textbullet};
	\node[black] at (-0.87, 1.61) {\textbullet};
	\node[black] at (-0.87, -2.39) {\textbullet};
	\node[black] at (-0.72, -1.95) {\textbullet};
	\node[black] at (-0.72, 2.05) {\textbullet};
	\node[black] at (0.23, -0.43) {\textbullet};
	\node[black] at (-1.72, 1.11) {\textbullet};
	\node[black] at (2.28, 1.11) {\textbullet};
	\node[black] at (-1.72, -2.89) {\textbullet};
	\node[black] at (2.28, -2.89) {\textbullet};
	\node[black] at (-0.03, -0.16) {\textbullet};
	\node[black] at (1.77, 1.06) {\textbullet};
	\node[black] at (-2.23, 1.06) {\textbullet};
	\node[black] at (1.77, -2.94) {\textbullet};
	\node[black] at (-2.23, -2.94) {\textbullet};
	\node[black] at (-0.79, 0.18) {\textbullet};
	\node[black] at (-1.34, -0.35) {\textbullet};
	\node[black] at (2.66, -0.35) {\textbullet};
	\node[black] at (-1.77, -1.49) {\textbullet};
	\node[black] at (2.23, -1.49) {\textbullet};
	\node[black] at (-1.77, 2.51) {\textbullet};
	\node[black] at (2.23, 2.51) {\textbullet};
	\node[black] at (-0.13, -0.43) {\textbullet};
	\node[black] at (-0.28, 0.22) {\textbullet};
	\node[black] at (1.69, 0.74) {\textbullet};
	\node[black] at (-2.31, 0.74) {\textbullet};
	\node[black] at (0.16, -0.40) {\textbullet};
	\node[black] at (1.73, -1.17) {\textbullet};
	\node[black] at (-2.27, -1.17) {\textbullet};
	\node[black] at (1.73, 2.83) {\textbullet};
	\node[black] at (-2.27, 2.83) {\textbullet};
	\end{tikzpicture}
	\caption{Visualization of 2D cell lists.}
	\label{fig:cell}
	\vspace{-0.5cm}
\end{figure}

\subsection*{Iterative Algorithm}
With cell lists, we find \textbf{ideal matches} for each pore massively in parallel. Next, the suggested matches are connected serially, ignoring already \emph{fully-connected} pores. This two-step process is repeated until no more valid matches remain, which only requires a few iterations. The overall performance of this algorithm is more than 10 times faster and is shown in Figure ~\ref{fig:algo}.

\begin{figure}[H]
	\begin{center}
		\includegraphics[width=\columnwidth]{../thesis/plots/comparison_time.png}
	\end{center}
	\caption{Run-times for increasing network sizes.}
	\label{fig:algo}
	\vspace{-0.5cm}
\end{figure}

\subsection*{Distributed Flow Solver}
Using the high-performance C library PETSc, we leverage the provided MPI-based iterative solvers to find the pore-pressures during flow simulation. We compare the run-times for this distributed solver with the existing serial version, and find performance improvements of about a factor of 5.

\end{multicols} % Don't change. Stops setting text in two-columns.
\end{document}
