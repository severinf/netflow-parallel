# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True
# cython: nonecheck=False

# distutils: language=c

import numpy as np

ctypedef int PetscErrorCode
# Petsc int is 32 bit by default
ctypedef int PetscInt
# Petsc scalar is double precision by default
ctypedef double PetscScalar

cdef extern from "../include/solve.h":
    PetscErrorCode init();
    PetscErrorCode solve(const PetscScalar *data, const PetscInt *col_indices, \
                    const PetscInt *row_ptr, const PetscScalar *rhs, 
                    PetscScalar *solution, PetscScalar rtol,
                    PetscInt maxiter, PetscInt n_rows);
    PetscErrorCode finalize();

def __init_petsc():
    return init()

def __finalize_petsc():
    return finalize()

# A: scipy.sparse.csr_matrix
# b: numpy.ndarray
def solve_py(A, b, PetscScalar rtol, PetscInt maxiter, PetscInt n_rows):
    ierr = __init_petsc()
    
    # Output: solution vector
    cdef PetscScalar [::1] solution = np.empty(n_rows, dtype=np.dtype("d"))
    
    # Memory views of numpy arrays (contiguous)
    # Datatypes: data: double, indptr, indices: int32
    cdef const PetscScalar [::1] data = A.data
    cdef const PetscScalar [::1] rhs  = b
    
    cdef const PetscInt [::1] row_ptr = A.indptr
    cdef const PetscInt [::1] col_indices = A.indices
    
    ierr = solve(&data[0], &col_indices[0], &row_ptr[0], &rhs[0], &solution[0], \
                  rtol, maxiter, n_rows)
	
    ierr = __finalize_petsc()
    # Convert memory view into np.ndarray and return
    return np.asarray(solution)
	
