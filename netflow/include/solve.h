/*
 * Header for PETSc based solver.
 * 
 * Severin Fritschi
 * December 2021
 */
 
#pragma once
#ifndef SOLVE_H
#define SOLVE_H

#include <petscksp.h>

PetscErrorCode init();

PetscErrorCode solve(const PetscScalar *data, const PetscInt *col_indices,
                    const PetscInt *row_ptr, const PetscScalar *rhs, 
                    PetscScalar *solution, PetscScalar rtol,
                    PetscInt maxiter, PetscInt n_rows);

PetscErrorCode finalize();

#endif /* SOLVE_H */
