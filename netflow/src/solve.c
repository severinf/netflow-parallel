/*
 * Distributed, iterative linear solver (GMRES + AMG pre-conditioning)
 * using PETSc and hypre(interfaced via PETSc).
 * 
 * Severin Fritschi
 * December 2021
 */

#include "../include/solve.h"

#include <stdio.h>  // printf
#include <stdlib.h>  // malloc

// Ignore macros in release build
#ifndef DEBUG
#undef  CHKERRQ
#define CHKERRQ(ierr)
#undef  CHKERRMPI
#define CHKERRMPI(ierr)
#endif

// Call this before running the function
PetscErrorCode init() {
    PetscErrorCode ierr;
    ierr = PetscInitialize(NULL, NULL, (char *)0, NULL); CHKERRQ(ierr);
    return ierr;
}

PetscErrorCode solve(const PetscScalar *data, const PetscInt *col_indices, 
               const PetscInt *row_ptr, const PetscScalar *rhs, 
               PetscScalar *solution, PetscScalar rtol,
               PetscInt maxiter, PetscInt n_rows) {
	
    PetscErrorCode ierr;
        
    const PetscMPIInt root = 0;
    // Fetch number of processes and local rank
    PetscMPIInt size, rank;
    ierr = MPI_Comm_size(PETSC_COMM_WORLD, &size); CHKERRMPI(ierr);
    ierr = MPI_Comm_rank(PETSC_COMM_WORLD, &rank); CHKERRMPI(ierr);
    
    // Calculate local size n_local directly for ALL ranks
    PetscInt n_local = n_rows / size;
    const PetscInt remainder = n_rows % size;
    PetscInt *n_locals = (PetscInt *) malloc(size * sizeof(PetscInt));
    
    // Initialize n_local array
    for (PetscMPIInt i = 0; i < size; ++i) {
        n_locals[i] = n_local + (i < remainder);
    }
    // Update n_local in the case that size does not divide n_rows
    n_local = n_locals[rank];
    
    // Initialize Vec objects
    Vec x;   // solution vector
    Vec bn;  // right-hand side vector
    ierr = VecCreateMPI(PETSC_COMM_WORLD, n_local, n_rows, &bn); CHKERRQ(ierr);
    ierr = VecDuplicate(bn, &x); CHKERRQ(ierr);
    
    // Assemble Vec on root rank
    if (rank == root) {
        for (PetscInt i = 0; i < n_rows; ++i) {
            ierr = VecSetValue(bn, i, rhs[i], INSERT_VALUES); CHKERRQ(ierr);
        }
    }
    ierr = VecAssemblyBegin(bn); CHKERRQ(ierr);
    ierr = VecAssemblyEnd(bn); CHKERRQ(ierr);
    
    // Initialize Mat object from csr arrays
    Mat An;
    // Set matrix values from arrays in csr format
    ierr = MatCreate(PETSC_COMM_WORLD, &An); CHKERRQ(ierr);
    ierr = MatSetSizes(An, n_local, n_local, n_rows, n_rows); CHKERRQ(ierr);
    ierr = MatSetFromOptions(An); CHKERRQ(ierr);
    ierr = MatSetUp(An); CHKERRQ(ierr);
    
    // Assemble Mat on root
    if (rank == root) {
        for (PetscInt i = 0; i < n_rows; ++i) {
            PetscInt start = row_ptr[i];
            PetscInt end   = row_ptr[i+1];
            
            ierr = MatSetValues(An, 1, &i, end - start, &col_indices[start],
                                    &data[start], INSERT_VALUES); CHKERRQ(ierr);
        }
    }
    ierr = MatAssemblyBegin(An, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    ierr = MatAssemblyEnd(An, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    
    // Initialize solver
    KSP ksp;
    ierr = KSPCreate(PETSC_COMM_WORLD, &ksp); CHKERRQ(ierr);
    ierr = KSPSetType(ksp, KSPGMRES); CHKERRQ(ierr);
    ierr = KSPSetOperators(ksp, An, An); CHKERRQ(ierr);
    
    // Initialize preconditioner
    PC pc;
    ierr = KSPGetPC(ksp, &pc); CHKERRQ(ierr);
    ierr = PCSetType(pc, PCHYPRE); CHKERRQ(ierr);
    ierr = PCHYPRESetType(pc, "boomeramg"); CHKERRQ(ierr);
    ierr = KSPSetTolerances(ksp, rtol, PETSC_DEFAULT, PETSC_DEFAULT, maxiter);
                                CHKERRQ(ierr);
    
    ierr = PCSetFromOptions(pc); CHKERRQ(ierr);
    ierr = KSPSetFromOptions(ksp); CHKERRQ(ierr);
    
    
    // Solve linear system
    ierr = KSPSolve(ksp, bn, x); CHKERRQ(ierr);
    
    // DEBUGGING: Monitor iteration number/convergence
    if (DEBUG) {
        PetscInt its;
        ierr = KSPGetIterationNumber(ksp, &its); CHKERRQ(ierr);
        PetscPrintf(PETSC_COMM_WORLD, "Iterations %d\n", its);
        const char *convergence_reason;
        ierr = KSPGetConvergedReasonString(ksp, &convergence_reason); CHKERRQ(ierr);
        PetscPrintf(PETSC_COMM_WORLD, "Reason: %s\n", convergence_reason);
        ierr = KSPView(ksp, PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);
    }
    // Displacements in global array (inscan of n_locals array)
    PetscMPIInt *displs = NULL;
    
    // Initialize displacements
    if (rank == root) {
        displs = (PetscMPIInt *) malloc(size * sizeof(PetscMPIInt));
        
        PetscMPIInt sum = 0;
        for (PetscMPIInt i = 0; i < size; ++i) {
            displs[i] = sum;
            sum += n_locals[i];
        }
    }
    // Obtain local array from Vec object
    const PetscScalar *x_array;
    ierr = VecGetArrayRead(x, &x_array); CHKERRQ(ierr);
    
    // Gather solution values on root (varying size)
    ierr = MPI_Gatherv(&x_array[0], n_local, MPI_DOUBLE, &solution[0],
                        &n_locals[0], &displs[0], MPI_DOUBLE, root,
                         PETSC_COMM_WORLD); CHKERRMPI(ierr);
    // Cleanup obtained pointer
    ierr = VecRestoreArrayRead(x, &x_array); CHKERRQ(ierr);
    // Cleanup
    if (rank == root) {
        free(displs);
    }
    free(n_locals);
    
    ierr = VecDestroy(&bn); CHKERRQ(ierr);
    ierr = VecDestroy(&x); CHKERRQ(ierr);
    ierr = MatDestroy(&An); CHKERRQ(ierr);
    
    ierr = KSPDestroy(&ksp); CHKERRQ(ierr);
    
    return ierr;
}

// Call this after using the function
PetscErrorCode finalize() {
    PetscErrorCode ierr;
    ierr = PetscFinalize(); CHKERRQ(ierr);
    return ierr;
}
