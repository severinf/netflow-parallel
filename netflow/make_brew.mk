PETSC_DIR=$(shell brew --prefix petsc)

include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules

# Linking flags needed in setup.py
export PETSC_LIB

.PHONY: all, cleanup

all: setup_brew.py
	python3 $< build_ext -i

debug: setup_brew.py
	python3 $< build_ext -i --debug

cleanup:
	${RM} -r build/
	${RM} *.so
	${RM} cwrapper/wrapper.c
	${RM} -r __pycache__
	${RM} -r ../netflow_serial/__pycache__
