## INSTALLATION:

0. Pre-requisites: 
    - (open)MPI compilers (mpicc, mpicxx), installed and located at /usr/bin/ 
    (otherwise, specify alternative directory in step 2.).
    - Python version should be 3.x (tested for 3.7 and 3.8).
    - Python libraries mpi4py, numpy, scipy, pyamg, h5py must be installed.

1. Clone PETSc repository from: https://gitlab.com/petsc/petsc

2. Run configuration script with following options:
./configure --with-cc=/usr/bin/mpicc --with-cxx=/usr/bin/mpicxx --with-fc=0 --download-hypre

3. Set environment flags PETSC_DIR and PETSC_ARCH as specified in output
of ./configure and run make all.

4. Run make all in this directory too. This should compile a dynamic library
file ending in .so

Now, you may simulate flow using routines solve_flow_inout() and
solve_flow_periodic() from netflow.py with PETSc solver specified
by the Solver.PETSC function argument.

## USAGE:

Refer to doc.ipynb in parent directory for basic usage of flow simulation
and network generation routines.
