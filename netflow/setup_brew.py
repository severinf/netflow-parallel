# Setup script for compilation of C wrapper using respective
# compiler flags. Run from Makefile exclusively.
#
# Severin Fritschi
# December 2021

import os, sys
import shutil
from setuptools import Extension, setup
from Cython.Build import cythonize

PETSC_DIR=""
PETSC_LIB=""

try:
    PETSC_DIR = os.environ["PETSC_DIR"]
    PETSC_LIB = os.environ["PETSC_LIB"]
except KeyError:
    print("Environment variables 'PETSC_DIR' not set.")
    sys.exit(1)

# MPI C compiler
try:
    mpi = os.popen('brew --prefix openmpi').read().strip()
except Exception:
    print("Please install openmpi with brew first.")
    sys.exit(1)

mpi += '/bin/mpicc'

compile_args = []
if "--debug" not in sys.argv:
    compile_args += ["-O3"]  # Release
compile_args += ["-std=c99"]  # For variable declarations in loop
compile_args += os.popen(mpi + " --showme:compile").read().strip().split(" ")
compile_args += ["-I" + PETSC_DIR + "/include"]

link_args = []
link_args += os.popen(mpi + " --showme:link").read().strip().split(" ")
link_args += PETSC_LIB.strip().split(" ")

sources = ["cwrapper/wrapper.pyx", "src/solve.c"]
# Set debug macro
macros = [('DEBUG', '0')]
if "--debug" in sys.argv:
    macros = [('DEBUG', '1')]
    # Remove argument again
    sys.argv.remove("--debug")

setup(
    name = "wrapper",
    ext_modules = cythonize(
                    [Extension("wrapper", 
                               sources=sources, 
                               extra_compile_args=compile_args,
                               extra_link_args=link_args,
                               define_macros=macros)], \
                    compiler_directives={'language_level': 3},
                  )
)
