import sys
sys.path.append('../')

from netflow import *

import open3d as o3d
import numpy as np

from typing import List

def coord_transform(pos: List[float], lb: float, ub: float):
    transform = lambda x: 10. / (ub - lb) * (2. * x - ub - lb)
    return np.array(list(map(transform, pos)))

def rotate_view(vis):
    ctr = vis.get_view_control()
    ctr.rotate(4.0, 0.0)
    return False


def main():
    dendro = netflow.load_network_from("../network/network.h5")
    #dendro = netflow.load_network_from("../gen/networks/dendro_1_1_1.h5")
    
    print(len(dendro.pores))
    print(len(dendro.throats))
    
    # Setup GUI
    vis = o3d.visualization.VisualizerWithKeyCallback()
    vis.create_window()
    # Add default primitives
    coordinate_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(
        size=0.1, origin=[0, 0, 0]
    )
    vis.add_geometry(coordinate_frame)  # coordinate frame
    
    aabb = o3d.geometry.AxisAlignedBoundingBox(
        min_bound=-10 * np.ones(3), max_bound=10 * np.ones(3)
    )
    aabb.color = [0.7, 0.7, 0.7]
    vis.add_geometry(aabb)  # bounding box
    
    # Colors
    black   = np.array([0.0, 0.0, 0.0])
    red     = np.array([0.8, 0.0, 0.0])
    blue    = np.array([0.0, 0.0, 0.8])
    magenta = np.array([1.0, 0.0, 1.0])
    
    n_zero_radius = 0
    for i, pore in enumerate(dendro.pores):
        radius = .6e4 * pore.r
        if pore.r < 1e-6:
            n_zero_radius += 1
            continue
        # Add sphere
        sphere = o3d.geometry.TriangleMesh.create_sphere(radius=radius, resolution=10)
        sphere.paint_uniform_color(blue)
        pos = coord_transform(pore.pos, dendro.lb[0], dendro.ub[0])
        sphere.translate(pos)
        
        vis.add_geometry(sphere)
    
    print("Did not render %d/%d pores because of too small radius" % (n_zero_radius, len(dendro.pores)))
    
    n = len(dendro.throats)
    n_zero_radius = 0
    for i, throat in enumerate(dendro.throats):
        if i == n:
            break
        radius = .8e3 * throat.r
        if throat.r < 1e-6:
            n_zero_radius += 1
            continue
        x1, x2 = netflow.throat_ends(throat,
                [ub-lb for lb,ub in zip(dendro.lb,dendro.ub)])
        x1_t = coord_transform(x1, dendro.lb[0], dendro.ub[0])
        x2_t = coord_transform(x2, dendro.lb[0], dendro.ub[0])
        height = np.linalg.norm(x1_t - x2_t)
        cylinder = o3d.geometry.TriangleMesh.create_cylinder(radius=radius, height=height, resolution=8, split=4)
        cylinder.paint_uniform_color(red)
        
        # normalized vectors
        cylinder_orient = np.array([0.0, 0.0, 1.0])
        cylinder_direct = (x1_t - x2_t) / height
        v = np.cross(cylinder_orient, cylinder_direct)
        s = np.linalg.norm(v)
        c = np.dot(cylinder_orient, cylinder_direct)
        skew = np.array([[0., -v[2], v[1]], \
                        [v[2], 0., -v[0]], \
                        [-v[1], v[0], 0.]])
        # Rotate cylinder to be aligned with throat direction
        R = np.eye(3) + skew + skew.dot(skew) / (1. + c)
        cylinder.rotate(R, np.array([0.0, 0.0, 0.0]))
        cylinder.translate((x1_t + x2_t) / 2.)
        
        vis.add_geometry(cylinder)
    
    print("Did not render %d/%d throats because of too small radius" % (n_zero_radius, n))
        
    while True:
        rotate_view(vis)
        if not vis.poll_events():
            break
        vis.update_renderer()
        
if __name__ == '__main__':
    main()
