import numpy as np

def cell_pos(pos):
    from math import floor
    return [floor(pos[i] + 3.) for i in range(2)]

def is_periodic(cellPos):
    return not all([2 <= cellPos[i] and cellPos[i] <= 3 for i in range(2)])

def print_point(x, y):
    x = np.round(x, 2)
    y = np.round(y, 2)
    print("\\node[black] at (%.2f, %.2f) {\\textbullet};" % (x, y))
    
if __name__ == '__main__':
    nInterior = 10
    nRest = 34
    
    interiorX = np.random.uniform(-2, 1, (nInterior, 1)) 
    interiorY = np.random.uniform(-1, 2, (nInterior, 1))
    
    interior = np.hstack((interiorX, interiorY))
    rest = np.random.uniform(-2, 2, (nRest, 2))
    
    points = np.vstack((interior, rest))
    
    for i in range(points.shape[0]):
        pos = points[i, :]
        cellPos = cell_pos(pos)
        x = pos[0]
        y = pos[1]
        
        print_point(x, y)
        if (is_periodic(cellPos)):
            # Left/right side
            if (x <= -1.):
                print_point(x + 4., y)
            elif (x >= 1.):
                print_point(x - 4., y)
            
            # Up/down side
            if (y <= -1.):
                print_point(x, y + 4.)
                if (x <= -1.):
                    print_point(x + 4., y + 4.)
                elif (x >= 1.):
                    print_point(x - 4., y + 4.)
            elif (y >= 1.):
                print_point(x, y - 4.)
                if (x <= -1.):
                    print_point(x + 4., y - 4.)
                elif (x >= 1.):
                    print_point(x - 4., y - 4.)
