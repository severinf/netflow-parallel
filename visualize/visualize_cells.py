import sys
sys.path.append('../')

from netflow import *

import open3d as o3d
import numpy as np

from typing import List

def coord_transform(pos: List[float], lb: float, ub: float):
    transform = lambda x: 10. / (ub - lb) * (2. * x - ub - lb)
    return np.array(list(map(transform, pos)))

def is_inside(cellList, triplet: List[int]):
    return all([(1 <= triplet[i] and triplet[i] < cellList.nCells[i] - 1) \
                for i in range(len(triplet))])
def main():
    dendro = netflow.load_network_from("network/dendro.h5")
    
    targetsize = [1, 1, 1]
    d = len(targetsize)
    # size of new network
    L = list(map(lambda lb,ub,i: (ub-lb)*i, dendro.lb, dendro.ub, targetsize))
    # Domain size including periodic buffer layers on all sides
    trueDomainSize = [L[i] + 2 * dendro.Lmax for i in range(d)]
    # Initialize cell-list; Place each pore in respective cell-set
    cellList = netflow.CellList(dendro.pores, trueDomainSize, dendro.Lmax, dendro, len(dendro.pores))
    
    # Setup GUI
    vis = o3d.visualization.VisualizerWithKeyCallback()
    vis.create_window()
    # Add default primitives
    coordinate_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(
        size=0.1, origin=[0, 0, 0]
    )
    vis.add_geometry(coordinate_frame)  # coordinate frame
    
    aabb = o3d.geometry.AxisAlignedBoundingBox(
        min_bound=-10 * np.ones(3), max_bound=10 * np.ones(3)
    )
    aabb.color = [0.7, 0.7, 0.7]
    vis.add_geometry(aabb)  # bounding box
    
    # Colors
    colors = [
        np.array([0.8, 0.0, 0.0]),
        np.array([0.0, 0.8, 0.0]),
        np.array([0.0, 0.0, 0.8]),
        np.array([0.8, 0.8, 0.0]),
        np.array([0.0, 0.8, 0.8]),
        np.array([0.8, 0.0, 0.8]),
        np.array([0.5, 0.3, 0.2]),
        np.array([0.3, 0.1, 0.5])
    ]
    
    color_dict = {}
    running_idx = 0
    n_zero_radius = 0
    for i, pore in enumerate(dendro.pores):
        radius = .6e4 * pore.r
        if pore.r < 1e-6:
            n_zero_radius += 1
            continue
        # Add sphere
        sphere = o3d.geometry.TriangleMesh.create_sphere(radius=radius, resolution=10)
        cellIdxTrip = cellList.pore_to_triplet(pore)
        if (is_inside(cellList, cellIdxTrip)):
            cellIdx = cellList.flatten(*cellIdxTrip)
            
            if cellIdx in color_dict:
                idx = color_dict[cellIdx]
            else:
                idx = running_idx
                color_dict[cellIdx] = idx
                running_idx += 1
                
            color = colors[idx]
        else:
            color = np.array([0.0, 0.0, 0.0])
        
        sphere.paint_uniform_color(color)
        pos = coord_transform(pore.pos, dendro.lb[0], dendro.ub[0])
        sphere.translate(pos)
        
        vis.add_geometry(sphere)
    
    print("Did not render %d/%d pores because of too small radius" % (n_zero_radius, len(dendro.pores)))
    
    while True:
        if not vis.poll_events():
            break
        vis.update_renderer()

if __name__ == '__main__':
    main()
