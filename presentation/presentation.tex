\documentclass[aspectratio=169]{beamer}

\usetheme{Warsaw}

% Use arrow vector
\def\vec{\mathaccent "017E\relax }

% Use \pause inside align environment
\makeatletter
\let\save@measuring@true\measuring@true
\def\measuring@true{%
	\save@measuring@true
	\def\beamer@sortzero##1{\beamer@ifnextcharospec{\beamer@sortzeroread{##1}}{}}%
	\def\beamer@sortzeroread##1<##2>{}%
	\def\beamer@finalnospec{}%
}

\makeatother
\usepackage{picture}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage[makeroom]{cancel}
\usepackage{tikz}
\usepackage{xcolor}
\usepackage[T1]{fontenc}
\usepackage{subfig}
\usepackage{caption}
\captionsetup[figure]{labelformat=empty} % no figure prefix

\title{\textbf{Generation and Simulation of Large-Scale \\ Flow Networks}}
\author{Severin Fritschi}
\date{16.12.2021}

\beamertemplatenavigationsymbolsempty
\setbeamertemplate{footline}[text line]{%
	\parbox{\linewidth}{\vspace*{-16pt}\centering\insertframenumber}}

% Colors
\definecolor{ao(english)}{rgb}{0.0, 0.5, 0.0}

\newcommand{\Magenta}[1]{\textcolor{magenta}{#1}}
\newcommand{\Orange}[1]{\textcolor{orange}{#1}}
\newcommand{\Red}[1]{\textcolor{red}{#1}}
\newcommand{\Green}[1]{\textcolor{ao(english)}{#1}}

\usefonttheme[onlymath]{serif}
\begin{document}
	\frame {
		\titlepage
		\centering
		Bachelor Thesis HS21
	}

	\frame {
		\frametitle{\textbf{Introduction}}
		\begin{tikzpicture}[remember picture,overlay]
		\node[xshift=-2.8cm,yshift=-3.5cm] at (current page.north east) {
			\includegraphics[width=0.5\textwidth]{../thesis/plots/base.png}
		};
		\node[xshift=-2.5cm,yshift=-6.0cm, align=center] at (current page.north east) {
			\textcolor{blue}{Scan of porous volume}
		};
		\end{tikzpicture}
		\textbf{Flow Networks}
		\begin{itemize}
			\pause
			\item Simplification of \emph{void-space} in \Orange{porous media} e.g. soil
			\pause
			\item Spherical \Orange{pores} connected by cylindrical \Orange{throats}
			\pause
			\item Used to study \emph{particle transport} during groundwater flow
			\pause
			\item Acquired by \emph{tomographic scans} of \Orange{small samples}
			\pause
			\item Need \Red{larger networks} for analysis of \emph{large-scale} phenomena
		\end{itemize}
		\pause
		\textbf{Netflow Python library}
		\begin{itemize}
			\pause
			\item Developed at IFD by Daniel W. Meyer
			\item Supports \emph{generation} and \emph{simulation} of flow networks
			\pause
			\item Larger realizations using \Orange{scanned network} \& derived \Orange{statistics}
			\pause
			\item \Red{Limited} to $\sim 10^6$ pores $\rightarrow$ \Green{parallel computing}
		\end{itemize}
	}

	\frame {
		\frametitle{\textbf{Flow Networks Visualized}}
		\begin{figure}
			\includegraphics[width=0.9\textwidth, height=0.7\textheight]{figures/basenet.png}
			\caption{\textcolor{blue}{3D render of network obtained from scan}}
		\end{figure}
	}

	\frame {
		% TODO: Visualization of generated network
		\frametitle{\textbf{Parallel Network Generation}}
		\begin{tikzpicture}[scale=0.7, xshift=15.6cm, yshift=-4cm, remember picture, overlay]
		\filldraw[draw=blue, fill=blue!10!white] (0,0,3) -- (3,0,3) -- (3,3,3) -- (0,3,3) -- cycle;
		\filldraw[draw=blue, fill=blue!10!white] (3,0,3) -- (3,0,0) -- (3,3,0) -- (3,3,3) -- cycle;
		\filldraw[draw=blue, fill=blue!10!white] (0,3,3) -- (3,3,3) -- (3,3,0) -- (0,3,0) -- cycle;
		\foreach \x in {0,...,3}
		{   \draw[draw=blue] (\x,0,3) -- (\x,3,3);
			\draw[draw=blue] (\x,3,3) -- (\x,3,0);
		}
		\foreach \x in {0,...,3}
		{   \draw[draw=blue] (3,\x,3) -- (3,\x,0);
			\draw[draw=blue] (0,\x,3) -- (3,\x,3);
		}
		\foreach \x in {0,...,3}
		{   \draw[draw=blue] (3,0,\x) -- (3,3,\x);
			\draw[draw=blue] (0,3,\x) -- (3,3,\x);
		}
		\filldraw[draw=red, fill=red!20!white] (2,2,3) -- (3,2,3) -- (3,3,3) -- (2,3,3) -- cycle;
		\filldraw[draw=red, fill=red!20!white] (3,2,3) -- (3,2,2) -- (3,3,2) -- (3,3,3) -- cycle;
		\filldraw[draw=red, fill=red!20!white] (2,3,3) -- (3,3,3) -- (3,3,2) -- (2,3,2) -- cycle;
		\node[xshift=-3.5cm,yshift=-6.1cm] at (current page.north east) {
			\textcolor{blue}{3D grid of network copies}
		};
		\end{tikzpicture}
		\textbf{Pore Distribution}
		\begin{itemize}
			\pause
			\item \Orange{Pores} are \emph{copied} from base network
			\item Arrange in 3D grid and \emph{perturb} pores randomly
			\pause
			\item Network \Orange{heterogeneity} is \emph{preserved}
		\end{itemize}
		\pause
		\textbf{Connecting Pores}
		\begin{itemize}
			\item Connect copied pores with \Orange{throats}
			\pause
			\item Each pore carries list of \emph{target throat lengths}
			\pause
			\item Among \emph{neighbors}, find \Orange{closest matches}
			\pause
			\item Repeat until \emph{no more matches left}
		\end{itemize}
		\vspace{0.5cm}
		\pause
		How to \Orange{find neighbors} of pore efficiently \& \Green{in parallel}?
	}

	\frame {
		\frametitle{\textbf{Cell Lists}}
		\begin{tikzpicture}[scale=0.78, xshift=15.6cm, yshift=-1cm, remember picture, overlay]
		\draw[black] (-3,-3) grid (3,3);
		\draw[draw=orange, fill=orange!10!white] (-3, -3) grid (2, -2) rectangle (-3, -3);
		\draw[draw=orange, fill=orange!10!white] (2, -3) grid (3, 3) rectangle (2, -3);
		\draw[draw=orange, fill=orange!10!white] (-3, 2) grid (2, 3) rectangle (-3, 2);
		\draw[draw=orange, fill=orange!10!white] (-3, -2) grid (-2, 2) rectangle (-3, -2);
		\draw[draw=blue, fill=blue!10!white] (-2, -1) grid (1, 2) rectangle (-2, -1);
		\node[anchor=north, black] at (-2.5, -3) {$0$};
		\node[anchor=north, black] at (-1.5, -3) {$1$};
		\node[anchor=north, black] at (-0.5, -3) {$2$};
		\node[anchor=north, black] at (0.5, -3) {$3$};
		\node[anchor=north, black] at (1.5, -3) {$4$};
		\node[anchor=north, black] at (2.5, -3) {$5$};
		\node[anchor=east, black] at (-3, -2.5) {$0$};
		\node[anchor=east, black] at (-3, -1.5) {$1$};
		\node[anchor=east, black] at (-3, -0.5) {$2$};
		\node[anchor=east, black] at (-3, 0.5) {$3$};
		\node[anchor=east, black] at (-3, 1.5) {$4$};
		\node[anchor=east, black] at (-3, 2.5) {$5$};
		\node[red] at (-0.5, 0.5) {\textbullet};
		\node[black] at (-1.04, 1.78) {\textbullet};
		\node[black] at (2.96, 1.78) {\textbullet};
		\node[black] at (-1.04, -2.22) {\textbullet};
		\node[black] at (2.96, -2.22) {\textbullet};
		\node[black] at (-0.10, 0.20) {\textbullet};
		\node[black] at (-0.13, -0.03) {\textbullet};
		\node[black] at (0.44, 0.23) {\textbullet};
		\node[black] at (-0.02, 1.70) {\textbullet};
		\node[black] at (-0.02, -2.30) {\textbullet};
		\node[black] at (-1.33, 1.17) {\textbullet};
		\node[black] at (2.67, 1.17) {\textbullet};
		\node[black] at (-1.33, -2.83) {\textbullet};
		\node[black] at (2.67, -2.83) {\textbullet};
		\node[black] at (0.63, -0.33) {\textbullet};
		\node[black] at (-1.05, 1.71) {\textbullet};
		\node[black] at (2.95, 1.71) {\textbullet};
		\node[black] at (-1.05, -2.29) {\textbullet};
		\node[black] at (2.95, -2.29) {\textbullet};
		\node[black] at (-1.33, 0.53) {\textbullet};
		\node[black] at (2.67, 0.53) {\textbullet};
		\node[black] at (-0.86, 0.14) {\textbullet};
		\node[black] at (-1.43, -0.91) {\textbullet};
		\node[black] at (2.57, -0.91) {\textbullet};
		\node[black] at (0.20, 1.58) {\textbullet};
		\node[black] at (0.20, -2.42) {\textbullet};
		\node[black] at (1.08, 1.53) {\textbullet};
		\node[black] at (-2.92, 1.53) {\textbullet};
		\node[black] at (1.08, -2.47) {\textbullet};
		\node[black] at (-2.92, -2.47) {\textbullet};
		\node[black] at (-0.83, -1.07) {\textbullet};
		\node[black] at (-0.83, 2.93) {\textbullet};
		\node[black] at (1.51, -0.19) {\textbullet};
		\node[black] at (-2.49, -0.19) {\textbullet};
		\node[black] at (1.25, 0.25) {\textbullet};
		\node[black] at (-2.75, 0.25) {\textbullet};
		\node[black] at (0.24, -1.89) {\textbullet};
		\node[black] at (0.24, 2.11) {\textbullet};
		\node[black] at (1.64, -1.59) {\textbullet};
		\node[black] at (-2.36, -1.59) {\textbullet};
		\node[black] at (1.64, 2.41) {\textbullet};
		\node[black] at (-2.36, 2.41) {\textbullet};
		\node[black] at (0.81, -0.14) {\textbullet};
		\node[black] at (0.99, 1.11) {\textbullet};
		\node[black] at (0.99, -2.89) {\textbullet};
		\node[black] at (1.56, 0.19) {\textbullet};
		\node[black] at (-2.44, 0.19) {\textbullet};
		\node[black] at (0.00, 0.29) {\textbullet};
		\node[black] at (-0.59, 1.98) {\textbullet};
		\node[black] at (-0.59, -2.02) {\textbullet};
		\node[black] at (0.69, 0.22) {\textbullet};
		\node[black] at (1.72, -0.51) {\textbullet};
		\node[black] at (-2.28, -0.51) {\textbullet};
		\node[black] at (1.46, 0.32) {\textbullet};
		\node[black] at (-2.54, 0.32) {\textbullet};
		\node[black] at (-1.95, -1.76) {\textbullet};
		\node[black] at (2.05, -1.76) {\textbullet};
		\node[black] at (-1.95, 2.24) {\textbullet};
		\node[black] at (2.05, 2.24) {\textbullet};
		\node[black] at (-1.17, 0.31) {\textbullet};
		\node[black] at (2.83, 0.31) {\textbullet};
		\node[black] at (0.55, -0.60) {\textbullet};
		\node[black] at (0.38, -1.83) {\textbullet};
		\node[black] at (0.38, 2.17) {\textbullet};
		\node[black] at (-0.87, 1.61) {\textbullet};
		\node[black] at (-0.87, -2.39) {\textbullet};
		\node[black] at (-0.72, -1.95) {\textbullet};
		\node[black] at (-0.72, 2.05) {\textbullet};
		\node[black] at (0.23, -0.43) {\textbullet};
		\node[black] at (-1.72, 1.11) {\textbullet};
		\node[black] at (2.28, 1.11) {\textbullet};
		\node[black] at (-1.72, -2.89) {\textbullet};
		\node[black] at (2.28, -2.89) {\textbullet};
		\node[black] at (-0.03, -0.16) {\textbullet};
		\node[black] at (1.77, 1.06) {\textbullet};
		\node[black] at (-2.23, 1.06) {\textbullet};
		\node[black] at (1.77, -2.94) {\textbullet};
		\node[black] at (-2.23, -2.94) {\textbullet};
		\node[black] at (-0.79, 0.18) {\textbullet};
		\node[black] at (-1.34, -0.35) {\textbullet};
		\node[black] at (2.66, -0.35) {\textbullet};
		\node[black] at (-1.77, -1.49) {\textbullet};
		\node[black] at (2.23, -1.49) {\textbullet};
		\node[black] at (-1.77, 2.51) {\textbullet};
		\node[black] at (2.23, 2.51) {\textbullet};
		\node[black] at (-0.13, -0.43) {\textbullet};
		\node[black] at (-0.28, 0.22) {\textbullet};
		\node[black] at (1.69, 0.74) {\textbullet};
		\node[black] at (-2.31, 0.74) {\textbullet};
		\node[black] at (0.16, -0.40) {\textbullet};
		\node[black] at (1.73, -1.17) {\textbullet};
		\node[black] at (-2.27, -1.17) {\textbullet};
		\node[black] at (1.73, 2.83) {\textbullet};
		\node[black] at (-2.27, 2.83) {\textbullet};
		
		\node[xshift=-2.7cm,yshift=-7.0cm] at (current page.north east) {
			\textcolor{blue}{2D cell lists}
		};
		\end{tikzpicture}
		\textbf{Cell Lists Datastructure}
		\begin{itemize}
			\pause
			\item Interaction of pores limited to \Orange{max. throat length} $L_m$
			\pause
			\item Divide domain into \Orange{cells} of side-length $L_m$
			\pause
			\item Determine \emph{cell-membership} based on pore \Orange{position}
			\pause
			\item Only consider \Green{\emph{adjacent}} cells to find matches
			\pause
			\item Pores are \emph{static}, only compute cell lists \Orange{once}
			\pause
			\item Allows straightforward \Green{parallel} match-finding
			\pause
			\item \emph{Efficient} removal of fully-connected pores
		\end{itemize}
	}

	\frame {
		\frametitle{\textbf{Parallel Network Generation}}
		\begin{tikzpicture}[scale=0.78, xshift=15.6cm, yshift=-2cm, remember picture, overlay]
		\draw[draw=blue, fill=blue!10!white] (-1,-1) grid (2,2) rectangle (-1,-1);
		\node[red] at (0.5, 0.5) {\textbullet};
		\node[black] at (1.5, 0.63) {\textbullet};
		\draw[draw=red, -stealth] (0.5, 0.5) -- (1.5, 0.63);
		\node[black] at (1.1, 1.7) {\textbullet};
		\draw[draw=red, -stealth] (0.5, 0.5) -- (1.1, 1.7);
		\node[black] at (0.3, 1.5) {\textbullet};
		\draw[draw=red, -stealth] (0.5, 0.5) -- (0.3, 1.5);
		\node[black] at (-0.2, 1.63) {\textbullet};
		\draw[draw=red, -stealth] (0.5, 0.5) -- (-0.2, 1.63);
		\node[black] at (-0.6, 0.16) {\textbullet};
		\draw[draw=red, -stealth] (0.5, 0.5) -- (-0.6, 0.16);
		\node[black] at (-0.5, -0.5) {\textbullet};
		\draw[draw=red, -stealth] (0.5, 0.5) -- (-0.5, -0.5);
		\node[black] at (0.25, -0.41) {\textbullet};
		\draw[draw=red, -stealth] (0.5, 0.5) -- (0.25, -0.41);
		\node[black] at (1.3, -0.5) {\textbullet};
		\draw[draw=red, -stealth] (0.5, 0.5) -- (1.3, -0.5);
		\node[black] at (-0.7, 0.8) {\textbullet};
		\draw[draw=red, -stealth] (0.5, 0.5) -- (-0.7, 0.8);
		\node[xshift=-2.5cm,yshift=-4.2cm] at (current page.north east) {
			\textcolor{blue}{Pore neighborhood}
		};
		\end{tikzpicture}
		\textbf{Find Matches in Parallel}
		\begin{itemize}
			\pause
			\item Distribute pores \emph{evenly} among \Orange{threads}
			\pause
			\item Threads find best matches and store them in \Orange{shared memory}
			\pause
			\item Pores are connected \emph{serially}, using matches
			\pause
			\item Matches may already be \Orange{fully-connected} $\rightarrow$ \Red{ignore}
			\pause
			\item Repeat with \Orange{left-out pores} until no more \emph{valid} matches
		\end{itemize}
		\pause
		Iterative procedure \Green{converges quickly}
		\pause
		\begin{table}[b]
			\centering
			\scalebox{0.7}{%
			\begin{tabular}{|c|c|c|}
				\hline
				\textbf{Iteration} & \textbf{Throats unrealized} & \textbf{Rel. percentage} \\
				\hline
				0 & 103464 & 100.0\% \\
				1 & 18690 & 18.1\% \\
				2 & 4329 & 4.2\% \\
				3 & 1096 & 1.1\% \\
				4 & 282 & 0.3\% \\
				5 & 73 & 0.1\% \\
				6 & 30 & 0.03\% \\
				7 & 27 & 0.026\% \\
				\hline
			\end{tabular}%
			}
		\end{table}
	}
	
	\frame {
		\frametitle{\textbf{Performance of Parallel Algorithm}}
		\begin{multicols}{2}
		\textbf{Strong Scaling}\\
		\Orange{Speedup} for increasing number of \emph{threads}
		\pause
		\begin{figure}[ht!]
			\raggedright
			\includegraphics[width=0.4\textwidth]{../thesis/plots/strong_scaling.png}
		\end{figure}
		\pause
		\textbf{Run-time Comparison}\\
		\Orange{Parallel} vs. \Orange{serial} implementation
		\pause
		\begin{figure}[ht!]
			\raggedright
			\includegraphics[width=0.4\textwidth]{../thesis/plots/comparison_time.png}
		\end{figure}
		\end{multicols}
	}

	\frame {
		\frametitle{\textbf{Parallel Flow Simulation}}
		\textbf{Network Flow}
		\begin{itemize}
			\pause
			\item Simulate flow through \Orange{periodic} and \Orange{in/out} networks
			\pause
			\item Study \emph{permeability} of networks
			\pause
			\item Solve \emph{sparse} \Orange{pressure system} for pore-pressures
			\pause
			\item \Red{Single-core} algebraic multi-grid (iterative) solver used
			\pause
			\item Replace by \Green{distributed} version
		\end{itemize}
		\pause
		\begin{tikzpicture}[remember picture,overlay]
		\node[xshift=-2.8cm,yshift=-3.5cm] at (current page.north east) {
			\includegraphics[width=0.3\textwidth]{figures/petsc.png}
		};
		\node[xshift=-2.8cm,yshift=-5.5cm] at (current page.north east) {
			\includegraphics[width=0.3\textwidth]{figures/hypre-logo.png}
		};
		\end{tikzpicture}
		\textbf{PETSc \& hypre}
		\begin{itemize}
			\pause
			\item \textbf{P}ortable, \textbf{E}xtensible \textbf{T}oolkit for \textbf{S}cientific \textbf{c}omputation
			\pause
			\item \Orange{C/C++ API} for MPI-based iterative solvers e.g. GMRES
			\pause
			\item Distributed AMG \emph{preconditioner} supplied via hypre
			\pause
			\item Call PETSc from Python using \Orange{Cython} wrapper
		\end{itemize}
	}
	
	\frame {
		\frametitle{\textbf{Performance of Parallel Solver}}
		\begin{multicols}{2}
			\textbf{Strong Scaling}\\
			\Orange{Speedup} for increasing number of \emph{ranks}
			\pause
			\begin{figure}[ht!]
				\raggedright
				\includegraphics[width=0.4\textwidth]{../thesis/plots/scaling_distributed.png}
			\end{figure}
			\pause
			\textbf{Run-time Comparison}\\
			\Orange{Parallel} vs. \Orange{serial} solvers
			\pause
			\begin{figure}[ht!]
				\raggedright
				\includegraphics[width=0.4\textwidth]{../thesis/plots/comparison_flow.png}
			\end{figure}
		\end{multicols}
	}

	\frame {
		\frametitle{\textbf{Results}}
		\textbf{Quality}
		\begin{itemize}
			\pause
			\item Noticeable \Green{performance improvements}
			\pause
			\item Parallel network generation realizes \Green{more throats}
			\pause
			\item Average \Orange{deviation} of realized throats \Green{smaller}
			\pause
			\item Compare \Orange{fluxes} computed from pressure solution of solvers
			\pause
			\item Results from PETSc \Green{identical} to others
		\end{itemize}
		\pause
		\textbf{Limitations}
		\begin{itemize}
			\pause
			\item \Red{High memory usage} of network generation
			\pause
			\item Distributed flow solver \Red{scales poorly} over \emph{multiple} nodes
		\end{itemize}
		\pause
		\textbf{Future Work}
		\begin{itemize}
			\pause
			\item Study \Orange{particle transport} through \emph{periodic} network samples
		\end{itemize}
	}

	\frame {
		\frametitle{\textbf{End}}
		\centering
		\Huge\textcolor{black}{\textbf{Questions?}}
	}
\end{document}